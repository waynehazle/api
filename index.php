<?php
/**
 * Created by Anurag.
 * user: Anurag Agrawal
 * Date: 11/26/15
 * Time: 11:40 PM
 * Main application file contains all the routes of the application
 */
//Report all errors
error_reporting(E_ALL);
ini_set('display_errors', 1);
require 'database/connection.php';
require 'classes/Bracket.php';
require 'Mailer/MailManager.php';
//Set the date and time
date_default_timezone_set('Asia/Kolkata');
require 'vendor/autoload.php';
$auth_id = "MANZNMMJG0NDY3YJGWMT";
$auth_token = "OTQwYWU5NGQ0Njg0ZDc5NjU3NWJiNzQ0NDY0MWM1";

session_start();
/**
 *
 * This is the main mail object used to send
 * emails using mandrill and php mailer
 * pass this variable with the route where you want to send any mail.
 */
$mail = new PHPMailer();
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("X-My-Custom-Header", "X-Another-Custom-Header"),
    "maxAge" => 1728000,
    "allowCredentials" => True,
    "allowMethods" => array("POST, GET"),
    "allowHeaders" => array("X-PINGOTHER")
);
$cors = new \CorsSlim\CorsSlim($corsOptions);
$m = new MailManager($mail);
$header = new Smtpapi\Header();
$header = new Smtpapi\Header();
$sendgrid = new SendGrid("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");
$email    = new SendGrid\Email();
//$sms = new RestAPI($auth_id, $auth_token);
/**
 * This block is the app configuration
 * don't change anything if its important
 */
$app = new \Slim\Slim(array(
    'mode' => 'development',
    'debug' => true,
    'log.enabled' => true,
    'log.level' => \Slim\Log::DEBUG,
    'log.writer' => new \Slim\Extras\Log\DateTimeFileWriter(array(
        'path' => './logs',
        'name_format' => 'Y-m-d',
        'message_format' => '%label% - %date% - %message%'
    )),
    'cookies.encrypt' => true,
    'cookies.lifetime' => '20 minutes',
    'base_url' => '/chooszing_api',
    'templates.path' => 'templates'
));

/*
 * Set cookies
 *
 */
$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'uno_session',
    'secret' => 'anurag11',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));
$app->add($cors);

$app->map('/', function ($code = '') use ($app) {
    $app->halt(403, 'Unauthorized Access');
})->via('GET', 'POST');


//Optional parameters
$app->get('/archive(/:year(/:month(/:day)))/', function ($year = 2010, $month = 12, $day = 05) {
    echo sprintf('%s-%s-%s', $year, $month, $day);
});

$app->get('/mistake', function () use ($app) {
    $app->halt(500, 'You shall not pass!');
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields)
{
    $app = \Slim\Slim::getInstance();
    $error = false;
    $error_fields = "";
    $request_params = $_REQUEST;
    // Handling PUT request params
    $data = json_decode($app->request()->getBody(), true);
    //$app->log->debug(isset($data['email']));
    /*if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        parse_str($app->request()->getBody(), $request_params);
    }*/
    foreach ($required_fields as $field) {
        if (!isset($data[$field]) || strlen(trim($data[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email)
{
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

// slim.after.dispatch would probably work just as well. Experiment
$app->hook('slim.after.router', function () use ($app) {
    $request = $app->request;
    $response = $app->response;
    $app->log->debug('Request path: ' . $request->getBody());
    $app->log->debug('Response status: ' . $response->getStatus());
    // And so on ...
});

$app->hook('slim.before', function () use ($app) {
    $posIndex = strpos($_SERVER['PHP_SELF'], '/index.php');
    $baseUrl = substr($_SERVER['PHP_SELF'], 0, $posIndex);
    $app->view()->appendData(array('baseUrl' => $baseUrl));
});

//Display custom page for error
$app->error(function (\Exception $e) use ($app) {
    $app->render('error.html');
});
//handle 404 error
$app->notFound(function () use ($app) {
    $app->log->debug('route -> ' . $app->request->getResourceUri() . ' - not found');
    echoRespnse(404, ['error'=>true, 'message'=>"Page not found."]);
});

/**
 * Slim can also handle error message and
 * send the appropriate HTTP responses
 */
$app->get('/denied', function () use ($app) {
    # Create the data
    $errorData = array('error' => 'Permission Denied');
    # Send a HTTP status of 403
    echoRespnse(403, ['error'=>true, 'message'=>"Access denied."]);
    # Or we can send
    $app->halt(403, 'Permission Denied');
});

$app->error(function (\Exception $e) use ($app) {
    echoRespnse(500, ['error'=>true, 'message'=>"Internal server error."]);
});

/**
 * API VERSION 1 ROUTES WILL COME HERE
 *
 */
/*require 'routes/api/v1/userRoutes.php';
require 'routes/api/v1/vehicleRoutes.php';
require 'routes/api/v1/utils.php';
require 'routes/api/v1/RideRoutes.php';*/
require 'routes/api/v1/landingRoutes.php';

//line needs to at the end
$app->run();
