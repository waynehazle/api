<?php

/**
 * Created by PhpStorm.
 * User: ola
 * Date: 12/1/15
 * Time: 12:53 PM
 */
class MailManager
{
    private $_mail;

    function __construct($mail) {
        $this->_mail = $mail;
        $this->_mail->isSMTP();                       // Set mailer to use SMTP
        $this->_mail->Host = 'smtp.mandrillapp.com';  // Specify main and backup SMTP servers
        $this->_mail->SMTPAuth = true;                // Enable SMTP authentication
        $this->_mail->Username = 'contact@unoride.com'; // SMTP username
        $this->_mail->Password = 'W9G93wm5pEH6aiUBQCRYXA';      // SMTP password
        $this->_mail->SMTPSecure = 'tls';             // Enable TLS encryption, `ssl` also accepted
        $this->_mail->Port = 587;                     // TCP port to connect to
        $this->_mail->From = 'contact@unoride.com';
        $this->_mail->FromName = 'Uno Ride';

    }

    public function sendRegistrationMail ($app, $email, $username, $activation_link) {
        $response = array();
        $this->_mail->addAddress($email);
        $this->_mail->isHTML(true);
        $this->_mail->Subject = 'Welcome to unoride.com';
        $body             = file_get_contents('mail_templates/new_signup.html');
        $body = str_replace('$username', $username, $body);
        $activation_link = "http://unoride.com/verify_me/".$email.'/'.$activation_link;
        $unsubscribe_link = "http://unoride.com/unsubscribe_me/".$email;
        $body = str_replace('$activate_url', $activation_link, $body);
        $body = str_replace('$unsubscribe_link', $unsubscribe_link, $body);

        $this->_mail->Body    = $body;
        $this->_mail->AltBody = 'to view this mail you need html supported browser';

        if(!$this->_mail->send()) {
            $response["error"] = true;
            $response["message"] = $this->_mail->ErrorInfo;
            echoRespnse(400, $response);
            $app->stop();
        } else {
            return true;
        }
        return $response;
    }

    public function sendReferralCode ($app, $email, $username, $referralCode) {
        $response = array();
        $this->_mail->addAddress($email);
        $this->_mail->isHTML(true);
        $this->_mail->Subject = 'Congratulations! Welcome to unoride.com';
        $body             = file_get_contents('mail_templates/referral_code.html');
        $body = str_replace('$username', $username, $body);
        $body = str_replace('$referral_code', $referralCode, $body);
        $unsubscribe_link = "http://unoride.com/unsubscribe_me/".$email;
        $body = str_replace('$unsubscribe_link', $unsubscribe_link, $body);
        $this->_mail->Body    = $body;
        $this->_mail->AltBody = 'to view this mail you need html supported browser';

        if(!$this->_mail->send()) {
            $response["error"] = true;
            $response["message"] = $this->_mail->ErrorInfo;
            echoRespnse(400, $response);
            $app->stop();
        } else {
            return true;
        }
        return $response;
    }

    public function sendUpdatePasswordMail($app, $email, $username, $token)
    {
        $response = array();
        $this->_mail->addAddress($email);
        $this->_mail->isHTML(true);
        $this->_mail->Subject = "Hello ". $username . ", Reset your password";
        $body             = file_get_contents('mail_templates/changePassword.html');
        //$body             = "Reset password. http://unoride.com/updatePassword";
        $update_password_link = "http://localhost:8888/uno_api_codebase/change_my_password/".$email."/".$token;
        $body = str_replace('$username', $username, $body);
        $body = str_replace('$activate_url', $update_password_link, $body);

        $this->_mail->Body    = $body;
        $this->_mail->AltBody = 'to view this mail you need html supported browser';

        if(!$this->_mail->send()) {
            $response["error"] = true;
            $response["message"] = $this->_mail->ErrorInfo;
            echoRespnse(400, $response);
            $app->stop();
        } else {
            return true;
        }
        return $response;
    }

    public function sendSavePasswordMail($app, $email, $username)
    {
        $response = array();
        $this->_mail->addAddress($email);
        $this->_mail->isHTML(true);
        $this->_mail->Subject = "Hello ". $username . ", Your password is updated";
        //$body             = file_get_contents('mail_templates/update_password.html');
        $body             = "Your password is updated successfully. Please save it somewhere for future. Have a great trip. :) :)";
        //$update_password = "http://unoride.com/updatePassword";
        //$body = str_replace('$username', $username, $body);
        //$body = str_replace('$update_password', $update_password, $body);

        $this->_mail->Body    = $body;
        $this->_mail->AltBody = 'to view this mail you need html supported browser';

        if(!$this->_mail->send()) {
            $response["error"] = true;
            $response["message"] = $this->_mail->ErrorInfo;
            echoRespnse(400, $response);
            $app->stop();
        } else {
            return true;
        }
        return $response;
    }

}