<?php
/**
 * Created by PhpStorm.
 * User: ola
 * Date: 2/6/16
 * Time: 3:47 PM
 */

$app->post('/api/v1/sendSMS', function () use ($app){
    $json = $app->request->getBody();
    $smsData = json_decode($json, true);
    $smsData = $smsData['sms'];
    $from_number = $smsData['from_number'];
    $to_number = $smsData['to_number'];
    $message_txt = $smsData['message_txt'];
    // Send a message
    $app->log->debug($smsData);
    $app->log->debug("-- sending sms from " . $from_number . " to " . $to_number . "and message text is " . $message_txt);
    $params = array(
        'src' => $from_number, // Sender's phone number with country code
        'dst' => $to_number, // Receiver's phone number with country code
        'text' => $message_txt // Your SMS text message
    );
    // Send message
    //$response = $sms->send_message($params);
    // Print the response
    echoRespnse(200, json_encode($response['response']));
});

$app->get('/api/v1/getLatLng/:id', function($placeID) use ($app) {
    $util = new Utils();
    $util->getLatLngFromAddressID($app, $placeID);
});


$app->get('/api/v1/makers', function() use ($app) {
    $vehicle = new Vehicle();
    $res = $vehicle->getMakers($app);
    echoRespnse(200, $res);
});

$app->post('/api/v1/profile_pic_upload', function() use ($app) {
    $storage = new \Upload\Storage\FileSystem('uploads/profile_pics');
    $file = new \Upload\File('profile_pic', $storage);
    $app->log->debug("anurag-------------------------");

    $userData = $app->request()->post('user');
    $app->log->debug($userData['clientID']);
// Optionally you can rename the file on upload
    $new_filename = uniqid();
    $file->setName($new_filename);
// Validate file upload
// MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
    $file->addValidations(array(
        // Ensure file is of type "image/png"
        new \Upload\Validation\Mimetype(array('image/png', 'image/gif', 'image/jpeg', 'image/jpg')),

        //You can also add multi mimetype validation
        //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

        // Ensure file is no larger than 5M (use "B", "K", M", or "G")
        new \Upload\Validation\Size('5M')
    ));

// Access data about the file that has been uploaded
    $data = array(
        'name'       => $file->getNameWithExtension(),
        'extension'  => $file->getExtension(),
        'mime'       => $file->getMimetype(),
        'size'       => $file->getSize(),
        'md5'        => $file->getMd5(),
        'dimensions' => $file->getDimensions()
    );

// Try to upload file
    try {
        // Success!
        $app->log->debug("uploading file with the object #");
        $app->log->debug($data);
        $file->upload();
        $user = new User();
        $user->insertProfilePic($userData['clientID'], $data['name']);
        echoRespnse(200, $data);
    } catch (\Exception $e) {
        // Fail!
        $errors = $file->getErrors();
        $app->log->debug("file upload fail with the message");
        $app->log->debug($errors);
        echoRespnse(400, ['error'=> true, 'message' => $e->getMessage()]);
    }
});


$app->post('/api/v1/lat_lng_from_placeid', function() use ($app) {
    $util = new Utils();
    $response = [];
    $locations = [];
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $place_data = $data['places'];
    $place_ids = $place_data['place_ids'];
    $app->log->debug($place_ids);
    foreach($place_ids as $place_id) {
        $locations[] = $util->getLatLngFromAddressID($app, $place_id);
        $response['locations'] = $locations;
    }
    echoRespnse(200, $response);
});


$app->get('/api/v1/lies_on_route', function() use($app) {
    $ride = new Ride();
    $points_response = $ride->getAllStopPointsOfRide($app, 60);
    $response =  \GeometryLibrary\PolyUtil::isLocationOnPath(
        ['lat' => 12.9299, 'lng' => 77.67540], // point array [lat, lng]
        $points_response
        , 0.05);
    echo $response;
});

$app->post('/api/v1/user_rating', function() use ($app) {
    $user = new User();
    $response = $user->submitUserRating($app);
    echoRespnse(200, $response);
});

$app->get('/api/v1/getRidePoints/:id', function($id) use ($app) {
    $ride = new Ride();
    $response = $ride->getAllStopPointsOfRide($app, $id);
    echoRespnse(200, $response);
});

$app->post('/sendMail', function() use ($app, $sendgrid, $email) {
    $email->addTo("agrawal.anu90@gmail.com")
        ->setFrom("contact@unoride.com")
        ->setSubject("Sending with SendGrid is Fun")
        ->setHtml('');
    $sendgrid->send($email);
});