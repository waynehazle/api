<?php

$app->post('/api/v1/register', function () use ($app) {
    $user = new User();
    $res = $user->registerUser($app);
    if (isset($res['status'])) {
        if ($res['status'] == 'USER_CREATED') {
            echoRespnse(200, $res);
        } else if ($res == 'USER_CREATION_FAILED') {
            echoRespnse(400, $res);
        } else if ($res['status'] == 'USER_ALREADY_EXISTED') {
            $res["error"] = true;
            $res["message"] = "Sorry, User with this email already exist";
            echoRespnse(400, $res);
        } else if ($res['status'] == 'USER_SUBSCRIPTION_FAILED') {
            $res["error"] = true;
            $res["message"] = "You are Successfully registered. (not subscribed yet.)";
            echoRespnse(200, $res);
        } else if ($res['status'] == 'INVALID_REFERRAL_CODE') {
            $res["error"] = true;
            $res["message"] = "Invalid referral code.";
            echoRespnse(400, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Unprocessable Entity";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/login', function () use ($app) {
    $user = new User();
    $res = $user->login($app);
    if (isset($res['status'])) {
        if ($res['status'] == 'LOGIN_SUCCESS') {
            echoRespnse(201, $res);
        } else if ($res['status'] == 'LOGIN_FAILED') {
            echoRespnse(400, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});


$app->post('/api/v1/create_ride_data', function () use ($app) {
    $user = new User();
    $res = $user->getRideCreateData($app);
    echoRespnse(200, $res);
});

$app->post('/api/v1/logout', function () use ($app) {
    $user = new User();
    $res = $user->logout($app);
    if (isset($res['status'])) {
        if ($res['status'] == 'LOGOUT_SUCCESS') {
            echoRespnse(200, $res);
        } else {
            $res["error"] = true;
            $res["message"] = "Something went wrong. Please try again";
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/activateuser', function () use ($app) {
    $user = new User();
    $res = $user->activateUser($app);
    if (isset($res['status'])) {
        if ($res['status'] == 'USER_ACTIVATED') {
            echoRespnse(200, $res);
        } else if ($res['status'] == 'ACTIVATION_FAILED') {
            $res["error"] = true;
            $res["message"] = "Something went wrong. Please try again";
            echoRespnse(500, $res);
        } else if ($res['status'] == 'ALREADY_ACTIVATED') {
            $res["error"] = true;
            $res["message"] = "User is already active.";
            echoRespnse(200, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/deactivateuser', function () use ($app) {
    $user = new User();
    $res = $user->deactivateUser($app);
    if (isset($res['status'])) {
        if ($res['status'] == 'USER_DEACTIVATED') {
            echoRespnse(200, $res);
        } else if ($res['status'] == 'DEACTIVATION_FAILED') {
            $res["error"] = true;
            $res["message"] = "Something went wrong. Please try again";
            echoRespnse(500, $res);
        } else if ($res['status'] == 'ALREADY_DEACTIVATED') {
            $res["error"] = true;
            $res["message"] = "User is already inactive.";
            echoRespnse(200, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/updateUserProfile', function () use ($app) {
    $user = new User();
    $res = $user->updateUserProfile($app);
    if (isset($res['status'])) {
        if ($res['status'] == 'PHONE_UPDATED') {
            $res["error"] = false;
            $res["message"] = "OTP has been sent to your mobile number.";
            echoRespnse(200, $res);
        } else if ($res['status'] == 'UPDATE_FAILED') {
            $res["error"] = true;
            $res["message"] = "Something went wrong. Please try again";
            echoRespnse(500, $res);
        } else if ($res['status'] == 'PROFILE_UPDATED') {
            $res["error"] = false;
            $res["message"] = "Profile details has been updated.";
            echoRespnse(200, $res);
        } else if ($res['status'] == 'INVALID_CLIENT_ID') {
            $res["error"] = true;
            $res["message"] = "Unable to find the user.";
            echoRespnse(500, $res);
        } else if ($res['status'] == 'MYSQL_ERROR') {
            $res["error"] = true;
            $res["message"] = $res['message'];
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/forgotPassword', function () use ($app, $m) {
    $user = new User();
    $res = $user->forgotPassword($app, $m);
    if (isset($res['status'])) {
        if ($res['status'] == 'RESET_PASSWORD_MAIL_SENT') {
            echoRespnse(200, $res);
        } else if ($res['status'] == 'USER_DOES_NOT_EXIST') {
            echoRespnse(500, $res);
        } else if ($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/savePassword', function () use ($app, $m) {
    $user = new User();
    $res = $user->savePassword($app, $m);
    if (isset($res['status'])) {
        if ($res['status'] == 'PASSWORD_SAVED') {
            echoRespnse(200, $res);
        } else if ($res['status'] == 'PASSWORD_SAVE_FAILED') {
            echoRespnse(500, $res);
        } else if ($res['status'] == 'UNAUTHORIZED_ACCESS') {
            echoRespnse(500, $res);
        } else if ($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->map('/change_my_password/:email/:token', function ($email, $token) use ($app, $m) {
    $user = new User();
    $isValidRequest = $user->validPasswordChangeRequest($app, $email, $token);
    $app->log->debug($isValidRequest);
    if ($isValidRequest == 'VALID') {
        $app->render('updatePassword.php', array('email' => $email, 'token' => $token), 200);
    } else {
        $app->redirect($app->config('base_url') . '/denied', 403);
    }

})->via('GET', 'POST')->name('change_my_password');


$app->get('/api/v1/get_profile_pic/:id', function ($id) use ($app, $m) {
    $user = new User();
    $user->getUserProfilePic($id, $app);
});

$app->get('/api/v1/user_rating/:id', function ($id) use ($app, $m) {
    $user = new User();
    $response = $user->getUserRating($app, $id);
    echoRespnse($response['code'], $response);
});