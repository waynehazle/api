<?php
/**
 * Created by PhpStorm.
 * User: ola
 * Date: 2/5/16
 * Time: 1:58 AM
 */
$app->post('/api/v1/addVehicle', function () use ($app) {
    $vehicle =  new Vehicle();
    $res = $vehicle->setVehicleDetails($app, false, []);
    if (isset($res['status'])){
        if ($res['status'] == 'VEHICLE_DETAILS_ADDITION_SUCCESSFUL') {
            echoRespnse(200, $res);
        } else if($res['status'] == 'DL_ADDITION_FAILED') {
            echoRespnse(400, $res);
        } else if($res['status'] == 'VEHICLE_DETAILS_ADDITION_FAILED') {
            echoRespnse(400, $res);
        } else if($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(400, $res);
        } else if ($res['status'] == 'VEHICLE_ALREADY_REGISTERED') {
            echoRespnse(400, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->get('/api/v1/vehicleExist/:regNumber', function ($regNumber) use ($app) {
    $vehicle = new Vehicle();
    $res = $vehicle->isVehicleExist($app, $regNumber);
    if (isset($res['status'])){
        if ($res['status'] == 'VEHICLE_ALREADY_REGISTERED') {
            echoRespnse(200, $res);
        } else if($res['status'] == 'VEHICLE_NOT_FOUND'){
            echoRespnse(200, $res);
        }else if($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/removeVehicle', function () use ($app) {
    $vehicle = new Vehicle();
    $res = $vehicle->removeVehicle($app);
    if (isset($res['status'])){
        if ($res['status'] == 'VEHICLE_REMOVED_SUCCESSFULLY') {
            echoRespnse(200, $res);
        } else if($res['status'] == 'VEHICLE_REMOVED_FAILED'){
            echoRespnse(500, $res);
        }else if($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->get('/api/v1/getVehicleDetails/:regNumber', function ($regNumber) use ($app) {
    $vehicle = new Vehicle();
    $res = $vehicle->getVehicleDetails($app, $regNumber);
    if (isset($res['status'])){
        if ($res['status'] == 'VEHICLE_DETAILS_FOUND') {
            echoRespnse(200, $res);
        } else if($res['status'] == 'VEHICLE_NOT_FOUND'){
            echoRespnse(500, $res);
        }else if($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->post('/api/v1/updateVehicleDetails', function () use ($app) {
    $vehicle =  new Vehicle();
    $res = $vehicle->updateVehicleDetails($app);
    if (isset($res['status'])){
        if ($res['status'] == 'VEHICLE_DETAILS_UPDATE_SUCCESSFUL') {
            echoRespnse(200, $res);
        } else if($res['status'] == 'VEHICLE_DETAILS_UPDATE_FAILED') {
            echoRespnse(400, $res);
        } else if($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(400, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});

$app->get('/api/v1/getAllVehicles/:clientID', function ($clientID) use ($app) {
    $vehicle = new Vehicle();
    $res = $vehicle->getAllVehicles($app, $clientID);
    if (isset($res['status'])){
        if ($res['status'] == 'VEHICLES_FOUND') {
            echoRespnse(200, $res);
        } else if($res['status'] == 'VEHICLES_NOT_FOUND'){
            echoRespnse(500, $res);
        }else if($res['status'] == 'MYSQL_ERROR') {
            echoRespnse(500, $res);
        }
    } else {
        $res["error"] = true;
        $res["message"] = "Something went wrong. Please try again";
        echoRespnse(500, $res);
    }
});