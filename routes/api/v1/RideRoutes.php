<?php
/**
 * Created by PhpStorm.
 * User: ola
 * Date: 2/9/16
 * Time: 8:21 PM
 */
$app->post('/api/v1/ride', function () use ($app, $m) {
    $app->log->debug('cllled');
    $ride = new Ride();
    $res = $ride->submitRide($app, $m);
    echoRespnse($res['code'],$res);
});

$app->post('/api/v1/publish_ride', function () use ($app, $m) {
    $ride = new Ride();
    $res = $ride->publishRide($app, $m);
    echoRespnse($res['code'],$res);
});

$app->get('/api/v1/searchRide', function () use ($app) {
    $ride = new Ride();
    $res = $ride->searchRides($app);
    echoRespnse($res['code'],$res);
});

$app->get('/api/v1/ride_subroutes/:id', function ($id) use ($app) {
    $ride = new Ride();
    $res = $ride->getRideRoutes($app, $id);
    echoRespnse(200,$res);
});