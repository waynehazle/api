<?php

  //GET all the movies
  $app->get('/movies/:title', function( $title ) use($app) {

    //http://dev.slime.com/api/v1/movies?q=star%20trek&limit=100

    $website = $app->config('base_url');
    $api = $app->urlFor('api_movies');
    

    $curl = new Curl\Curl();
    $curl->get($website.$api, array(
      'q' => $title
    ));

    print_r( $curl->response );
  

  });