<?php
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
class Db
{
    private $_connection;
    private static $_instance; //The single instance
    private $_host = 'chooszing.com';
    private $_username = 'dev_dbuser';
    private $_password = 'anurag11';
    private $_database = 'dev_chooszing';

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    // Constructor
    private function __construct()
    {
        try {
            $this->_connection = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
            // Check connection
            $connected = true;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {
    }
    // Get mysql pdo connection
    public function getConnection()
    {
        return $this->_connection;
    }
}
/**
 * And you can use it as such in a class
 * */

/*class Post {
    public function __construct(){
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }
    public function getPosts()
    {
        try {
            $sql = "SELECT * FROM posts";
            foreach ($this->_dbh->query($sql) as $row) {
                var_dump($row);
            }
            $this->_dbh = null;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}*/