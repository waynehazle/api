/**
 * Created by ola on 12/31/15.
 */
$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $("#header").addClass("whiteHeader");
    } else {
        $("#header").removeClass("whiteHeader");
    }
});


$(document).ready(function() {
    $('form').parsley();
});
function checkValidRfCode (event) {
    $('#info').html('Checking referral code validity...');
    var rfCode = event.currentTarget.value;
    $.ajax({
        url: 'api/v1/validateRfCode/'+rfCode,
        data: {
            format: 'json'
        },
        error: function(data) {
            var result = JSON.parse(data.responseText);
            $('#info').html('<p>'+result['message']+' <a href="" class="text-danger"><i class="fa fa-times"></i> </a> </p>');
        },
        dataType: 'jsonp',
        success: function(data) {
            var result = JSON.parse(data.responseText);
            $('#info').html('<p>'+result['message']+'</p>');
        },
        type: 'GET'
    });
}

function earlySignup () {
    preloader = new $.materialPreloader({
        element: '#custom-modal'
    });
    preloader.on();
    var url = 'api/v1/register';
    var formdata = $('#earlySignupForm').serialize();
    $.ajax({
        url: url,
        data: formdata,
        type: 'POST',
        success: function(data) {
            preloader.off();
            var response =  data;
            if (response.error) {
                $('#signup_error').html('<span class="text-danger">'+response.message+'</span>');
            } else if (typeof response.error === 'undefined') {
                $('#signup_error').html('<span class="text-danger">Something went wrong. Please try again!</span>');
            } else {
                Custombox.close();
                $('#user_email').html(response.uno_user_email);
                $('#uno_user_id').val(response.uno_user_id);
                $('#mail_confirmation_link').click();
            }
        },
        error: function(data) {
            preloader.off();
            $('#signup_error').html('<span class="text-danger">Something went wrong. Please try again!</span>');
        }
    });
}

function earlySignup2 () {
    preloader = new $.materialPreloader({
        element: '#mainBody'
    });
    preloader.on();
    var url = 'api/v1/register';
    var formdata = $('#earlySignupForm').serialize();
    $.ajax({
        url: url,
        data: formdata,
        type: 'POST',
        success: function(data) {
            preloader.off();
            var response =  data;
            if (response.error) {
                $('#signup_error').html('<span class="text-danger">'+response.message+'</span>');
            } else if (typeof response.error === 'undefined') {
                $('#signup_error').html('<span class="text-danger">Something went wrong. Please try again!</span>');
            } else {
                $('#user_email').html(response.uno_user_email);
                $('#uno_user_id').val(response.uno_user_id);
                $('#mail_confirmation_link').click();
            }
        },
        error: function(data) {
            preloader.off();
            $('#signup_error').html('<span class="text-danger">Something went wrong. Please try again!</span>');
        }
    });
}

function changeEmail () {
    $('#user_email').hide();
    $('#txtBox').show();
    $('#txtBox').val($('#user_email').text());
    $('#email_changed').val('YES');
}

function resendConfirmationMail(event) {
    preloader = new $.materialPreloader({
        element: '#confirmation_modal'
    });
    preloader.on();
    var user_id = $('#uno_user_id').val();
    var email_changed = $('#email_changed').val();
    var user_email = '';
    if (email_changed === 'YES') {
        user_email = $('#txtBox').val();
    } else {
        user_email = $('#user_email').text();
    }
    var url = 'api/v1/resendConfirmation';
    var requestData = {
        'user_id' : user_id,
        'user_email': user_email,
        'email_changed': email_changed

    };
    $.ajax({
        url: url,
        data: requestData,
        type: 'POST',
        success: function(data) {
            preloader.off();
            $('#reSuccess').removeClass('hidden');
            $('#resent_success').html(data.message);
        },
        error: function(data) {
            $('#reError').removeClass('hidden');
            preloader.off();
            $('#resent_error').html(data.responseJSON.message);
        }
    });
}