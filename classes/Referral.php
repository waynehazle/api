<?php

/**
 * Created by PhpStorm.
 * User: ola
 * Date: 12/22/15
 * Time: 12:25 PM
 */
require_once 'database/connection.php';
class Referral extends Db
{
    public function __construct(){
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }

    /**
     * Validating referral code
     * If the referral code is there in db, it is a valid referral code
     * @param String $referral_code referral code
     * @return boolean
     */
    public function isValidReferralCode($referral_code) {
        $response = array();
        $mysqli = $this->_dbh;
        $stmt = $mysqli->prepare("SELECT id from referral_codes WHERE code = ? AND referral_codes.used_referrals < referral_codes.max_referral_limit");
        $stmt->bind_param("s", $referral_code);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        if (($num_rows <= 0)) {
            // Required field(s) are missing or empty
            // echo error json and stop the app
            $app = \Slim\Slim::getInstance();
            $response["error"] = true;
            $response["message"] = 'Sorry! Invalid referral code.';
            echoRespnse(400, $response);
            $app->stop();
        } else {
            return $num_rows > 0;
        }
        return $response;
    }

    public function generateReferralCode () {
        return strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10));
    }


    public function add_new_referral_code($user_id) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;
        try {
            $referral_code = $this->generateReferralCode();
            $sql = "INSERT INTO referral_codes (code, `associate_user`, `max_referral_limit`, `used_referrals`, status) VALUES ('{$referral_code}', '{$user_id}', 10, 0, 'ACTIVE')";
            if ($mysqli->query($sql) === TRUE) {
                return 'REFERRAL_CODE_CREATED';
            } else {
                return 'REFERRAL_CODE_CREATE_FAILED';
            }
        } catch (Exception $e) {
            $response["error"] = true;
            $response["message"] = 'Sorry! '. $e->getMessage();
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function useReferralCode ($referral_code) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;
        try {
            $sql = "UPDATE referral_codes SET referral_codes.used_referrals  = referral_codes.used_referrals+1 WHERE referral_codes.used_referrals < referral_codes.max_referral_limit AND code = '{$referral_code}';";
            $mysqli->query($sql);
            $app->log->debug('affected rows for use code'. $mysqli->affected_rows);
            if ($mysqli->affected_rows) {
                return 'REFERRAL_CODE_USED';
            } else {
                return 'REFERRAL_CODE_USE_FAILED';
            }
        } catch (Exception $e) {
            $response["error"] = true;
            $response["message"] = 'Sorry! '. $e->getMessage();
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function giveReferralKarmaPointsToUser ($referral_code) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;
        try {
            $user_id = $this->getUserIdByReferralCode($referral_code);
            $sql = "SELECT id from users_referral_points WHERE  user_id = '{$user_id}' LIMIT 1";
            $result = $mysqli->query($sql);
            $app->log->debug('user entry found ' . $result->num_rows);
            if ($result->num_rows) {
                //user exist in the table update the  record
                $sql = "UPDATE users_referral_points SET referral_points = referral_points+100, referral_counts = referral_counts+1 WHERE user_id ='{$user_id}'";
                $mysqli->query($sql);
                if ($mysqli->affected_rows) {
                    return 'REFERRAL_POINTS_GIVEN';
                } else {
                    return 'REFERRAL_POINTS_GIVE_FAILED';
                }
            } else {
                //user doesn't exist in the table create the first entry
                $sql = "INSERT INTO users_referral_points (`user_id`, `referral_points`, `referral_counts`) VALUES ('{$user_id}',100,users_referral_points.referral_counts+1)";
                if ($mysqli->query($sql) ===  TRUE) {
                    return 'REFERRAL_POINTS_GIVEN';
                } else {
                    return 'REFERRAL_POINTS_GIVE_FAILED';
                }
            }
        } catch (Exception $e) {
            $response["error"] = true;
            $response["message"] = 'Sorry! '. $e->getMessage();
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    /**
     * This function will be used to get the user id based on the referral code.
     * @param $referral_code
     * @return array
     * @throws \Slim\Exception\Stop
     */
    public function getUserIdByReferralCode($referral_code) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;;
        $sql = "SELECT referral_codes.associate_user FROM referral_codes WHERE referral_codes.code = '{$referral_code}' LIMIT 1";
        $result = $mysqli->query($sql);
        if ($result) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $user_id = $row['associate_user'];
            $app->log->debug('code is associated with user'. $user_id);
            return $user_id;
        } else {
            $response["error"] = true;
            $response["message"] = 'Sorry! Unable to map user to this referral code.';
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function getReferralCodeByUserId($user_id) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;;
        $sql = "SELECT referral_codes.code FROM referral_codes WHERE referral_codes.associate_user = '{$user_id}' LIMIT 1";
        $result = $mysqli->query($sql);
        if ($result) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $referral_code = $row['code'];
            $app->log->debug('User id is associated with code'. $referral_code);
            return $referral_code;
        } else {
            $response["error"] = true;
            $response["message"] = 'Sorry! Unable to get referral code for this user.';
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function insertReferralUsersMapping($user_id, $referrer_id) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;;
        $sql = "INSERT INTO referral_mapping (`user_id`, `referrer_id`) VALUES ('{$user_id}', '{$referrer_id}')";
        if ($mysqli->query($sql) ===  TRUE) {
            $app->log->debug('REFERRAL_MAPPING_INSERTED');
            return 'REFERRAL_MAPPING_INSERTED';
        } else {
            $response["error"] = true;
            $response["message"] = 'REFERRAL_MAPPING_INSERT_FAILED';
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function getReferralCountsByUserId($user_id) {
        $app = \Slim\Slim::getInstance();
        $response = array();
        $mysqli = $this->_dbh;;
        $sql = "SELECT COUNT(referral_mapping.referrer_id) AS total_referrals FROM referral_mapping WHERE referral_mapping.referrer_id = '{$user_id}'";
        $result = $mysqli->query($sql);
        if ($result) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $referral_count = $row['total_referrals'];
            $app->log->debug('Total Referrals'. $referral_count);
            return intval($referral_count);
        } else {
            $response["error"] = true;
            $response["message"] = 'Sorry! Unable to get referral codes.';
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

}