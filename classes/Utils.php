<?php

/**
 * Created by PhpStorm.
 * User: ola
 * Date: 2/6/16
 * Time: 4:29 PM
 */
class Utils
{

    public function sendSMS($app, $smsObj, $to, $from, $message_txt) {
        $app->log->debug("-- sending sms from " . $from . " to " . $to . "and message text is " . $message_txt);
        $params = array(
            'src' => $from, // Sender's phone number with country code
            'dst' => (int)('91'.$to), // Receiver's phone number with country code
            'text' => $message_txt // Your SMS text message
        );
        // Send message
        $response = $smsObj->send_message($params);
        // Print the response
        return ($response['response']);
    }


    function getLatLngFromAddressID ($app, $addressID) {
        $app->log->debug("https://maps.googleapis.com/maps/api/place/details/json?placeid={$addressID}&key=AIzaSyBtHuYBqyHJoCd2E0cZBshiIKwWhPiuDzU");
        $place = file_get_contents("https://maps.googleapis.com/maps/api/place/details/json?placeid={$addressID}&key=AIzaSyBtHuYBqyHJoCd2E0cZBshiIKwWhPiuDzU");
        $output= json_decode($place);
        if ($output->status == 'OK') {
            $latitude = $output->result->geometry->location->lat;
            $longitude = $output->result->geometry->location->lng;
            $formatted_address = $output->result->formatted_address;

            $response = [
                "lat" => $latitude,
                "lng" => $longitude,
                "formatted_addr" => $formatted_address
            ];
        } else {
            $res = [
                "error"=> true,
                "message"=> "Unable to find the locations latitudes and longitudes",
            ];
           echoRespnse(400, $res);
            $app->stop();
        }
        //echoRespnse(200, $response);
        return $response;
    }

    function get_distance_between_points($app, $latitude1, $longitude1, $latitude2, $longitude2) {
        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$latitude1.','.$longitude1.'&destinations='.$latitude2.','.$longitude2.'&key=AIzaSyBtHuYBqyHJoCd2E0cZBshiIKwWhPiuDzU';
        $app->log->debug($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geoloc = json_decode(curl_exec($ch), true);
        if ($geoloc['status']=='OK'){
            $fDistance=(float)preg_replace('/[^\d\.]/','',$geoloc['rows'][0]['elements'][0]['distance']['text']);
            $travel_time=(float)preg_replace('/[^\d\.]/','',$geoloc['rows'][0]['elements'][0]['duration']['text']);
        }
        else
            $fDistance=0;
        $app->log->debug($fDistance . ' time is ' . $travel_time);
        return [
            "travel_distance" => $fDistance,
            "travel_time" => $travel_time
            ];
    }

    function get_way_points_data ($app, $origin_lat, $origin_lng, $destination_lat, $destination_lng, $waypoints) {
        $waypoints_string = '';
        $waypoints_array = [];
        $points = [];
        foreach($waypoints as $waypoint) {
            $waypoints_string = $waypoints_string . $waypoint['lat'] . ',' . $waypoint['lng'] . '|';
        }
        $url = "https://maps.googleapis.com/maps/api/directions/json?origin={$origin_lat},{$origin_lng}&destination={$destination_lat},{$destination_lng}&waypoints=optimize:true|{$waypoints_string}&key=AIzaSyBtHuYBqyHJoCd2E0cZBshiIKwWhPiuDzU";
        $app->log->debug($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $waypointResult = json_decode(curl_exec($ch), true);
        if ($waypointResult['status']=='OK'){
            if (isset($waypointResult['routes'][0]['legs'])) {
                foreach($waypointResult['routes'][0]['legs'] as $leg) {
                    //@TODO
                    $res['distance'] = $leg['distance'];
                    $res['duration'] = $leg['duration'];
                    $res['start_address'] = $leg['start_address'];
                    $res['start_location'] = $leg['start_location'];
                    $res['end_address'] = $leg['end_address'];
                    $res['end_location'] = $leg['end_location'];
                    array_push($points, $res);
                }
            }
            $waypoints_array['stop_overs'] = $points;
            $waypoints_array['waypoints_order'] = $waypointResult['routes'][0]['waypoint_order'];
        }
        else {}
        return $waypoints_array;
    }

    function calculateCO2Emission ($vehicle_type,$distance) {
        $co2value = 0;
        $unit = 'g CO2';
        /**
         * Calculation for diesel vehicle
         * Diesel:
         * 1 liter of diesel weighs 835 grammes. Diesel consist for 86,2% of carbon, or 720 grammes of carbon per liter diesel.
         * In order to combust this carbon to CO2, 1920 grammes of oxygen is needed. The sum is then 720 + 1920 = 2640 grammes of CO2/liter diesel.
         * An average consumption of 5 liters/100 km then corresponds to 5 l x 2640 g/l / 100 (per km) = 132 g CO2/km.
         *
         *
         *
         *
         *
         * Petrol:1 liter of petrol weighs 750 grammes. Petrol consists for 87% of carbon, or 652 grammes of carbon per liter of petrol.
         * In order to combust this carbon to CO2, 1740 grammes of oxygen is needed. The sum is then 652 + 1740 = 2392 grammes of CO2/liter of petrol.
         * An average consumption of 5 liters/100 km then corresponds to 5 l x 2392 g/l / 100 (per km) = 120 g CO2/km.
         *
         *
         * LPG:1 liter of LPG weighs 550 grammes. LPG consists for 82,5% of carbon, or 454 grammes of carbon per liter of LPG.
         * In order to combust this carbon to CO2, 1211 grammes of oxygen is needed. The sum is then 454 + 1211 = 1665 grammes of CO2/liter of LPG.
         * An average consumption of 5 liters / 100 km then corresponds to 5 l x 1665 g/l / 100 (per km) = 83 g of CO2/km.
         *
         *
         *
         * CNG:CNG is a gaseous fuel (natural gas), stored under high pressure. Consequently, the consumption can be expressed in Nm3/100km,
         * but also in kg/100km. Nm3 stands for a cubic meter under normal conditions (1 atm and 0 ° C). Consumption of natural gas vehicles is, however,
         * most often expressed in kg/100km.
         * two cases here Low-calorific CNG: An average consumption of 5 kg / 100 km then corresponds to 5 kg x 2252 g/kg = 113 g CO2/km.
         *
         * High-calorific:1 kg of H-gas consists for 72,7% of carbon, or 727 grammes of carbon per kg of H-gas. In order to combust this carbon to CO2, 1939 grammes of oxygen is needed.
         * The sum is then 727 + 1939 = 2666 grammes of CO2/kg of H-gas.
         *
         * An average consumption of 4.2 kg / 100 km then corresponds to 4,2 kg x 2666 g/kg = 112 g of CO2/km
         */

        switch ($vehicle_type) {
            case 'diesel':
                $co2value = 132 * ($distance);
                break;
            case 'petrol':
                $co2value = 120 * ($distance);
                break;
            case 'LPG':
                $co2value = 83 * ($distance);
                break;
            case 'CNG':
                $co2value = 112 * ($distance);
                break;
        }

        return [
            "code"=> 200,
            "value"=> $co2value,
            "status"=> "SUCCESS",
            "unit" => $unit
        ];
    }
}

//api for waypoints call
//https://maps.googleapis.com/maps/api/directions/json?origin=Adelaide,SA&destination=Adelaide,SA&waypoints=optimize:true|Barossa+Valley,SA|Clare,SA|Connawarra,SA|McLaren+Vale,SA&key=AIzaSyBtHuYBqyHJoCd2E0cZBshiIKwWhPiuDzU