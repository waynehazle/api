<?php
/**
 * Created by PhpStorm.
 * User: Arpit
 * Date: 25-Jan-16
 * Time: 11:56 PM
 */

require_once 'database/connection.php';

class Vehicle extends Db
{
    public function __construct()
    {
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }

    public function setVehicleDetails ($app, $nonRouteCall, $vehicle)
    {
        if (!$nonRouteCall) {
            $json = $app->request->getBody();
            $vehicle = json_decode($json, true);
            $app->log->debug($vehicle);
            $vehicle_data = $vehicle['vehicle'];
        } else {
            $vehicle_data = $vehicle;
        }
        $user = new User();
        $app->log->debug("before userID: ". $vehicle_data['clientID']);
        $userID = $user->getUserId($vehicle_data['clientID']);
        $app->log->debug("userID: ".$userID);

        $response = array();
        $mysqli = $this->_dbh;

        $rto_location = isset($vehicle_data['rto_location']) ? $mysqli->real_escape_string($vehicle_data['rto_location']) : NULL;
        $no_of_seats = isset($vehicle_data['no_of_seats']) ? $mysqli->real_escape_string($vehicle_data['no_of_seats']) : NULL;
        $no_of_owner = isset($vehicle_data['no_of_owner']) ? $mysqli->real_escape_string($vehicle_data['no_of_owner']) : NULL;
        $insurance_type = isset($vehicle_data['insurance_type']) ? $mysqli->real_escape_string($vehicle_data['insurance_type']) : NULL;
        $insurance_expiry_date = isset($vehicle_data['insurance_expiry_date']) ? $mysqli->real_escape_string($vehicle_data['insurance_expiry_date']) : NULL;
        $registration_date = isset($vehicle_data['registration_date']) ? $mysqli->real_escape_string($vehicle_data['registration_date']) : NULL;
        $type = isset($vehicle_data['type']) ? $mysqli->real_escape_string($vehicle_data['type']) : NULL;
        $registration_number = $mysqli->real_escape_string($vehicle_data['registration_number']);
        $dl_number = isset($vehicle_data['dl_number']) ? $mysqli->real_escape_string($vehicle_data['dl_number']) : NULL;
        $vehicle_type = $mysqli->real_escape_string($vehicle_data['vehicle_type']);

        //remove all white spaces
        $registration_number = preg_replace('/\s+/', '', $registration_number);
        $vehicleExist = $this->isVehicleExist($app, $registration_number);

         if ($vehicleExist['status'] == 'VEHICLE_ALREADY_REGISTERED') {
             $response['status'] = "VEHICLE_ALREADY_REGISTERED";
             $response['message'] = "Vehicle with this registration number already exists";
             $response['vehicle_exist'] = 'true';
             $response['error'] = 'true';
         } else {
             if (!array_key_exists("fuel_type",$vehicle_data)) {
                 $fuel_type = '';
             } else {
                 $fuel_type = $mysqli->real_escape_string($vehicle_data['fuel_type']);
             }

             if (!array_key_exists("color",$vehicle_data)) {
                 $color = '';
             } else {
                 $color = $mysqli->real_escape_string($vehicle_data['color']);
             }

             if (!array_key_exists("manufacturing_year",$vehicle_data)) {
                 $manufacturing_year = '';
             } else {
                 $manufacturing_year = $mysqli->real_escape_string($vehicle_data['manufacturing_year']);
             }

             if (!array_key_exists("agency_id",$vehicle_data)) {
                 $agency_id = 0;
             } else {
                 $agency_id = $mysqli->real_escape_string($vehicle_data['agency_id']);
             }

             if (!array_key_exists("make",$vehicle_data)) {
                 $make = '';
             } else {
                 $make = $mysqli->real_escape_string($vehicle_data['make']);
             }

             if (!array_key_exists("model",$vehicle_data)) {
                 $model = '';
             } else {
                 $model = $mysqli->real_escape_string($vehicle_data['model']);
             }
             //print_r($user_data);

             try
             {
                 $sql = "INSERT INTO vehicle (`userid`, `rto_location`,`fuel_type`,`color`,`no_of_seats`,`no_of_owner`,`insurance_type`,`insurance_expiry_date`, `manufacturing_year`,`registration_date`, `type`,`agency_id`, `registration_number`,`make`,`model`, `vehicle_type` ,`status`) VALUES ('{$userID}', '{$rto_location}','{$fuel_type}','{$color}', '{$no_of_seats}', '{$no_of_owner}','{$insurance_type}', '{$insurance_expiry_date}','{$manufacturing_year}','{$registration_date}','{$type}','{$agency_id}', '{$registration_number}','{$make}','{$model}', '{$vehicle_type}' , '0')";
                 $app->log->debug($sql);
                 if($mysqli->query($sql) === true)
                 {
                     $vehicle_id = $mysqli->insert_id;
                     if ($dl_number != null) {
                         $sql = "UPDATE user_personal_details SET dl_number = '{$dl_number}' WHERE userid = '{$userID}'";
                         $app->log->debug($sql);
                         $mysqli->query($sql);
                         $app->log->debug("Affected rows: " . $mysqli->affected_rows);

                         if($mysqli->affected_rows)
                         {
                             $response['status'] = "VEHICLE_DETAILS_ADDITION_SUCCESSFUL";
                             $response['message'] = "Vehicle details addition and DL addition are successful";
                             $response['error'] = "false";
                             $response['vehicle_id'] = $vehicle_id;
                         }
                         else
                         {
                             $response['status'] = "DL_ADDITION_FAILED";
                             $response['message'] = "DL addition failed";
                             $response['error'] = 'true';
                             $response['vehicle_id'] = $vehicle_id;
                         }
                     } else {
                         $response['status'] = "VEHICLE_DETAILS_ADDITION_SUCCESSFUL";
                         $response['message'] = "Vehicle details addition and DL addition are successful";
                         $response['error'] = "false";
                         $response['vehicle_id'] = $vehicle_id;
                     }
                 }
                 else
                 {
                     $response['status'] = "VEHICLE_DETAILS_ADDITION_FAILED";
                     $response['message'] = "Vehicle details addition and DL addition failed";
                     $response['error'] = "true";
                 }
             }
             catch(Exception $e)
             {
                 $response['status'] = "MYSQL_ERROR";
                 $response['message'] = "Sorry. Query failed: ". $e->getMessage();
                 $response['error'] = "true";
             }
         }
        return $response;
    }

    /*public function insertAgencyVehicle ($agency_id, $vehicle_id, $app)
    {
        $mysqli = $this->_dbh;
        $sql = "INSERT INTO agency_vehicle_map (`agency_id`,`vehicle_id`) VALUES ('{$agency_id}', '{$vehicle_id}')";

        $app->log->debug($sql);
        if ($mysqli->query($sql))
        {
            return true;
        }
        else
        {
            return false;
        }
    }*/

    public function isVehicleExist($app, $regNumber)
    {
        $response = array();
        $mysqli = $this->_dbh;

        //$reg_number = $mysqli->real_escape_string($vehicle['user']['registration_number']);
        $app->log->debug("Registration number:". $regNumber);

        //remove all white spaces
        $regNumber = preg_replace('/\s+/', '', $regNumber);

        //search for the agency with the given details
        $sql = "SELECT * from vehicle WHERE registration_number='{$regNumber}'";
        $app->log->debug($sql);

        try
        {
            $result = $mysqli->query($sql);
            // Check results
            if ($result->num_rows)
            {
                //vehicle exists
                $row = mysqli_fetch_assoc($result);
                $app->log->debug($row[0]);
                $response['status'] = "VEHICLE_ALREADY_REGISTERED";
                $response['message'] = "Vehicle with this registration number already exists";
                $response['vehicle_exist'] = 'true';
                $response['error'] = 'true';
                $response['vehicle_id'] = $row['id'];
            }
            else
            {
                //vehicle does not exist
                $response['status'] = "VEHICLE_NOT_FOUND";
                $response['message'] = "Vehicle not found";
                $response['vehicle_exist'] = 'false';
                $response['error'] = 'true';
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['vehicle_exist'] = 'false';
            $response['error'] = 'false';
        }

        return $response;
    }


    public function getVehicleDetails($app, $regNumber)
    {
        $response = array();
        $mysqli = $this->_dbh;

        //$reg_number = $mysqli->real_escape_string($vehicle['user']['registration_number']);
        $app->log->debug("Registration number:". $regNumber);

        //remove all white spaces
        $regNumber = preg_replace('/\s+/', '', $regNumber);

        //search for the agency with the given details
        $sql = "SELECT * FROM vehicle WHERE registration_number='{$regNumber}' LIMIT 1";
        $app->log->debug($sql);

        try
        {
            if($result = $mysqli->query($sql))
            {
                if($result->num_rows)
                {
                    $row = mysqli_fetch_assoc($result);
                    $response['status'] = "VEHICLE_DETAILS_FOUND";
                    $response['vehicle_details'] = $row;
                    $response['message'] = "Vehicle details found";
                    $response['error'] = 'true';
                }
                else
                {
                    $response['status'] = "VEHICLE_NOT_FOUND";
                    $response['vehicle_details'] = null;
                    $response['message'] = "Vehicle details not found";
                    $response['error'] = 'false';
                }
            }
            else
            {
                $response['status'] = "VEHICLE_NOT_FOUND";
                $response['vehicle_details'] = null;
                $response['message'] = "Vehicle details not found";
                $response['error'] = 'false';
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['vehicle_details'] = null;
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'false';
        }
        return $response;
    }

    public function removeVehicle($app)
    {
        $json = $app->request->getBody();
        $vehicle = json_decode($json, true);
        $app->log->debug($vehicle);

        $response = array();
        $mysqli = $this->_dbh;

        $reg_number = $mysqli->real_escape_string($vehicle['vehicle']['registration_number']);
        //remove all white spaces
        $reg_number = preg_replace('/\s+/', '', $reg_number);
        $app->log->debug("Registration number:". $reg_number);

        try
        {
            $sql = "UPDATE vehicle SET status = '0' WHERE registration_number = '{$reg_number}'";
            $app->log->debug($sql);

            $mysqli->query($sql);
            $app->log->debug("Affected rows: " . $mysqli->affected_rows);

            if($mysqli->affected_rows)
            {
                $response['status'] = "VEHICLE_REMOVED_SUCCESSFULLY";
                $response['message'] = "Vehicle is removed successfully";
                $response['error'] = 'true';
            }
            else
            {
                $response['status'] = "VEHICLE_REMOVED_FAILED";
                $response['message'] = "Vehicle removal unsuccessful";
                $response['error'] = 'false';
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query Failed. " . $e->getMessage();
            $response['error'] = 'false';
        }

        return $response;
    }

    public function updateVehicleDetails ($app)
    {
        $json = $app->request->getBody();
        $vehicle = json_decode($json, true);
        $app->log->debug($vehicle);

        $user = new User();

        $vehicle_data = $vehicle['vehicle'];

        $vehicleID = $vehicle_data['vehicleID'];

        $app->log->debug("before userID: ". $vehicle_data['clientID']);
        $userID = $user->getUserId($vehicle_data['clientID']);
        $app->log->debug("userID: ".$userID);

        $response = array();
        $mysqli = $this->_dbh;

        $rto_location = isset($vehicle_data['rto_location']) ? $mysqli->real_escape_string($vehicle_data['rto_location']) : NULL;
        $no_of_seats = isset($vehicle_data['no_of_seats']) ? $mysqli->real_escape_string($vehicle_data['no_of_seats']) : NULL;
        $no_of_owner = isset($vehicle_data['no_of_owner']) ? $mysqli->real_escape_string($vehicle_data['no_of_owner']) : NULL;
        $insurance_type = isset($vehicle_data['insurance_type']) ? $mysqli->real_escape_string($vehicle_data['insurance_type']) : NULL;
        $insurance_expiry_date = isset($vehicle_data['insurance_expiry_date']) ? $mysqli->real_escape_string($vehicle_data['insurance_expiry_date']) : NULL;
        $registration_date = isset($vehicle_data['registration_date']) ? $mysqli->real_escape_string($vehicle_data['registration_date']) : NULL;
        //$type = isset($vehicle_data['type']) ? $mysqli->real_escape_string($vehicle_data['type']) : NULL;
        $registration_number = isset($vehicle_data['registration_number']) ? $mysqli->real_escape_string($vehicle_data['registration_number']) : NULL;
        //$dl_number = isset($vehicle_data['dl_number']) ? $mysqli->real_escape_string($vehicle_data['dl_number']) : NULL;
        $vehicle_type = isset($vehicle_data['vehicle_type']) ? $mysqli->real_escape_string($vehicle_data['vehicle_type']) : NULL;

        //remove all white spaces
        $registration_number = preg_replace('/\s+/', '', $registration_number);
        if (!array_key_exists("fuel_type",$vehicle_data)) {
            $fuel_type = '';
        } else {
            $fuel_type = $mysqli->real_escape_string($vehicle_data['fuel_type']);
        }

        if (!array_key_exists("color",$vehicle_data)) {
            $color = '';
        } else {
            $color = $mysqli->real_escape_string($vehicle_data['color']);
        }

        if (!array_key_exists("manufacturing_year",$vehicle_data)) {
            $manufacturing_year = '';
        } else {
            $manufacturing_year = $mysqli->real_escape_string($vehicle_data['manufacturing_year']);
        }

        if (!array_key_exists("agency_id",$vehicle_data)) {
            $agency_id = 0;
        } else {
            $agency_id = $mysqli->real_escape_string($vehicle_data['agency_id']);
        }

        if (!array_key_exists("make",$vehicle_data)) {
            $make = '';
        } else {
            $make = $mysqli->real_escape_string($vehicle_data['make']);
        }

        if (!array_key_exists("model",$vehicle_data)) {
            $model = '';
        } else {
            $model = $mysqli->real_escape_string($vehicle_data['model']);
        }
        //print_r($user_data);

        try
        {
            $sql = "UPDATE vehicle SET rto_location = '{$rto_location}', fuel_type = '{$fuel_type}', color = '{$color}', no_of_seats = '{$no_of_seats}', no_of_owner = '{$no_of_owner}', insurance_type = '{$insurance_type}', insurance_expiry_date = '{$insurance_expiry_date}', manufacturing_year = '{$manufacturing_year}', registration_date = '{$registration_date}', agency_id = '{$agency_id}', registration_number = '{$registration_number}', make = '{$make}', model = '{$model}', vehicle_type = '{$vehicle_type}' WHERE userid = '{$userID}' AND id = '{$vehicleID}'";
            $app->log->debug($sql);

            $mysqli->query($sql);
            $app->log->debug("Affected rows: " . $mysqli->affected_rows);

            if($mysqli->affected_rows)
            {
                $response['status'] = "VEHICLE_DETAILS_UPDATE_SUCCESSFUL";
                $response['message'] = "Vehicle details update is successful";
                $response['error'] = "true";
            }
            else
            {
                $response['status'] = "VEHICLE_DETAILS_UPDATE_FAILED";
                $response['message'] = "Vehicle details update failed";
                $response['error'] = "false";
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = "false";
        }
        return $response;
    }

    public function getAllVehicles($app, $clientID)
    {
        $response = array();
        $mysqli = $this->_dbh;

        $app->log->debug("ClientID: ". $clientID);
        $user = new User();
        $userID = $user->getUserId($clientID);
        $app->log->debug("UserID: ". $userID);

        $sql = "SELECT vehicle.id,`rto_location`,`fuel_type`,`color`,`no_of_seats`,`no_of_owner`,`insurance_type`,`insurance_expiry_date`, `manufacturing_year`,`registration_date`, `registration_number`,`make`,`model`, `vehicle_type` FROM vehicle LEFT JOIN user_personal_details ON vehicle.userid = user_personal_details.userid WHERE vehicle.userid = '{$userID}'";
        $app->log->debug($sql);

        try
        {
            if($result = $mysqli->query($sql))
            {
                if($result->num_rows)
                {
                    $app->log->debug($result->num_rows);
                    $num_of_vehicles = $result->num_rows;
                    //$vehicleResult = mysqli_fetch_all($result,MYSQLI_ASSOC);
                    while ($row = $result->fetch_assoc()) {
                        $vehicleResult[] = $row;
                    }
                    $app->log->debug($vehicleResult);

                    $response['status'] = "VEHICLES_FOUND";
                    $response["results_count"] = $num_of_vehicles;
                    $response["dl_number"] = $this->getDLNumber($app, $userID);
                    $response['vehicle_details'] = $vehicleResult;
                    $response['message'] = "Found all vehicle details";
                    $response['error'] = 'false';
                }
                else
                {
                    $response['status'] = "VEHICLES_NOT_FOUND";
                    $response["results_count"] = 0;
                    $response['vehicle_details'] = [];
                    $response['message'] = "Vehicle for this user do not exist";
                    $response['error'] = 'true';
                }
            }
            else
            {
                $response['status'] = "VEHICLES_NOT_FOUND";
                $response["results_count"] = 0;
                $response['vehicle_details'] = [];
                $response['message'] = "Vehicle for this user do not exist";
                $response['error'] = 'true';
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['vehicle_details'] = [];
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'true';
        }
        return $response;
    }

    public function getMakers ($app) {
        //$app->log->debug("fetching makers");
        $return_arr = [];
        $mysqli = $this->_dbh;
        try {
            $app->log->debug("fetching makers");
            $sql = 'SELECT name, id FROM maker WHERE active="Yes" AND type IN (1,4)';
            $result = $mysqli->query($sql);
            //$return_arr = mysqli_fetch_all($result,MYSQLI_ASSOC);
            while ($row = $result->fetch_assoc()) {
                $row['models'] = $this->getModels($app, $row['id']);
                $return_arr[] = $row;
            }

        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        $app->log->debug($return_arr);
        return $return_arr;
    }

    public function getModels ($app, $maker_id) {
        //$app->log->debug("fetching makers");
        $return_arr = array();
        $mysqli = $this->_dbh;
        try {
            $app->log->debug("fetching models");
            $sql = "SELECT name, id as model_id FROM model WHERE active='Yes' AND maker_id= {$maker_id} AND model.type=4";
            $result = $mysqli->query($sql);
            //$return_arr = mysqli_fetch_all($result,MYSQLI_ASSOC);
            while ($row = $result->fetch_assoc()) {
                $return_arr[] = $row;
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        return $return_arr;
    }

    public function getDLNumber ($app, $userID) {
        //$app->log->debug("fetching makers");
        $dl = '';
        $mysqli = $this->_dbh;
        try {
            $app->log->debug("fetching models");
            $sql = "SELECT dl_number FROM user_personal_details WHERE userid={$userID}";
            $result = $mysqli->query($sql);
            //$return_arr = mysqli_fetch_all($result,MYSQLI_ASSOC);
            while ($row = $result->fetch_assoc()) {
                $dl = $row['dl_number'];
            }
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        return $dl;
    }
}