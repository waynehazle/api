<?php

/**
 * Created by PhpStorm.
 * User: ola
 * Date: 10/6/15
 * Time: 4:10 PM
 */
require_once "database/connection.php";

class Ride
{
    public function __construct(){
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }

    public function submitRide ($app, $m) {
        $mysqli = $this->_dbh;
        $user = new User();
        /**
         * @TODO insert stopovers mapping.
         * ride type 1 - direct / 0 - stops
         * if ride type is 0 then it has stopovers so insert the stopovers mapping.
         */
        $ride_locations = []; //will store all the point's lat lng on the ride route to be sent in ride creation response for map display
        $app->log->debug('inside ride submit');
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        $rideData = $data['ride'];
        $app->log->debug(gettype($data));
        $vehicle_data = $rideData['vehicle'];
        $app->log->debug($vehicle_data);
        #fetch vehicle id
        if ($vehicle_data['id'] == 0 ) {
            $veh = new Vehicle();
            $insert_vehicle = $veh->setVehicleDetails($app, true, $vehicle_data);
            $app->log->debug($insert_vehicle);
            $vehicle_id = $insert_vehicle['vehicle_id'];
            $app->log->debug($insert_vehicle);
        } elseif ($vehicle_data['id'] > 0 ) {
            $vehicle_id = $vehicle_data['id'];
        }
            if ($vehicle_id > 0 ) {
            //calculate lat lng for to and from
            $util = new Utils();
            $source_location = $util->getLatLngFromAddressID($app, $rideData['tripInfo']['source_place_id']);
            $destination_location = $util->getLatLngFromAddressID($app, $rideData['tripInfo']['destination_place_id']);

            $app->log->debug($source_location);

            $app->log->debug($destination_location);
            $ride_distance_data = $util->get_distance_between_points($app, $source_location['lat'], $source_location['lng'], $destination_location['lat'], $destination_location['lng']);
            $ride_distance = $ride_distance_data['travel_distance'];
            $ride_time = $ride_distance_data['travel_time'];
            $app->log->debug($ride_distance);
            $user_data = $user->getUser($app,$rideData['clientID']);
            $user_email = $user_data['user_email'];
            $user_id = $user_data['id'];

            $driver_id = $mysqli->real_escape_string($user_id);
            $type = $mysqli->real_escape_string($rideData['tripInfo']['type']);
            $source = $mysqli->real_escape_string($source_location['formatted_addr']);
            $source_latitude = $mysqli->real_escape_string($source_location['lat']);
            $source_longitude = $mysqli->real_escape_string($source_location['lng']);
            $destination = $mysqli->real_escape_string($destination_location['formatted_addr']);
            $destination_latitude = $mysqli->real_escape_string($destination_location['lat']);
            $destination_longitude = $mysqli->real_escape_string($destination_location['lng']);
            $departure_date = strtotime($mysqli->real_escape_string($rideData['tripInfo']['departure_time']));
            $no_of_seats_offered = $mysqli->real_escape_string($rideData['tripInfo']['seats']);
            $price = $mysqli->real_escape_string(preg_replace('/\D/', '', $rideData['tripInfo']['price']));
                if (isset($rideData['preferences']) && isset($rideData['preferences']['stop_over_ids']) && sizeof($rideData['preferences']['stop_over_ids']) > 0) {
                    $app->log->debug($rideData['preferences']['stop_over_ids']);
                    $type = 1; //this ride has stopovers
                    $stopovers = [];
                    foreach($rideData['preferences']['stop_over_ids'] AS $stopover_id) {
                        $stopover_location = $util->getLatLngFromAddressID($app, $stopover_id);
                        $stopovers[] = $stopover_location;
                    }
                    $waypoints_result = $util->get_way_points_data($app, $source_latitude, $source_longitude, $destination_latitude, $destination_longitude, $stopovers);
                    $app->log->debug($waypoints_result);
                } else {

                }
                if (isset($rideData['preferences']['detour_time'])){
                    $rideData['preferences']['detour_time'] =  $mysqli->real_escape_string(preg_replace('/\D/', '', $rideData['preferences']['detour_time']));
                }
            $preferences = json_encode($rideData['preferences']);
            $app->log->debug($preferences);
                $app->log->debug($price);
            //print_r($user_data);

            try{
                $uno_ride_id = $this->generateUnoRideId();
                $query = "INSERT INTO ride_detail (`user_id`, `driver_id`, `vehicle_id`, `type`, `source`, `source_latitutde`, `source_longitude`, `destination`, `destination_latitude`, `destinantion_longitude`, `departure_date`, `no_of_seats_offered`, `price`, `preferences`, `uno_ride_id`) VALUES ('{$user_id}','{$driver_id}','{$vehicle_id}','{$type}', '{$source}', '{$source_latitude}','{$source_longitude}', '{$destination}','{$destination_latitude}','{$destination_longitude}','{$departure_date}','{$no_of_seats_offered}','{$price}','{$preferences}', '{$uno_ride_id}')";
                if ($mysqli->query($query) === TRUE && $mysqli->insert_id){
                    $ride_id = $mysqli->insert_id;
                    if ($ride_id) {
                        /**
                         * Single or direct ride with no stopovers
                         */
                        if ($type == 0) {
                            $this->insertSubRoutes($ride_id, $source, $source_latitude, $source_longitude, $destination, $destination_latitude, $destination_longitude, $ride_distance, $price, $ride_time);
                            $res['lat'] = $source_latitude; $res['lng'] = $source_longitude;
                            array_push($ride_locations, $res);
                            $res['lat'] = $destination_latitude; $res['lng'] = $destination_longitude;
                            array_push($ride_locations, $res);
                        } else {
                            foreach ($waypoints_result['stop_overs'] as $sub_route) {
                                $res['lat'] = $sub_route['start_location']['lat']; $res['lng'] = $destination_longitude;
                                $this->insertSubRoutes($ride_id, $sub_route['start_address'], $sub_route['start_location']['lat'], $sub_route['start_location']['lng'], $sub_route['end_address'], $sub_route['end_location']['lat'], $sub_route['end_location']['lng'], preg_replace("/[^0-9,.]/", "", $sub_route['distance']['text']), $price, preg_replace("/[^0-9,.]/", "", $sub_route['duration']['text']));
                                $test['lat'] = $sub_route['start_location']['lat'];$test['lng'] = $sub_route['start_location']['lng'];
                                array_push($ride_locations, $test);
                            }
                            $test['lat'] = (float)$destination_latitude; $test['lng'] = (float)$destination_longitude;
                            array_push($ride_locations, $test);
                        }
                        $co2 = $util->calculateCO2Emission('diesel', $ride_distance);
                        return [
                            "code"=> 200,
                            "message"=> "Ride details has been inserted.",
                            "status"=> "SUCCESS",
                            "ride"=> [
                                "ride_id"=> $ride_id,
                                "uno_ride_id"=> $uno_ride_id,
                                "locations"=> $ride_locations,
                                "total_distance"=> round($ride_distance),
                                "co2_saved"=> round((($co2['value'])*($no_of_seats_offered-1))/1000)
                            ]
                        ];
                    } else {
                        return [
                            "code"=> 500,
                            "message"=> 'Unable to insert ride details.',
                            "status"=> "ERROR"
                        ];
                    }
                }
            }
            catch(Exception $e)
            {
                //We will catch ANY exception that the try block will throw
                //echo $e;
                return [
                    "code"=> 500,
                    "message"=> $e->getMessage(),
                    "status"=> "ERROR"
                ];
            }
        }

    }

    function insertStopoverMappings ($stopover_data) {

    }

    function insertSubRoutes ($ride_id, $source, $source_lat, $source_lng, $destination, $destination_lat, $destination_lng, $distance, $price, $time) {
        $app = \Slim\Slim::getInstance();
        $mysqli = $this->_dbh;
        $route_price = (float)((float)$price * $distance);
        $util = new Utils();
        $co2_data = $util->calculateCO2Emission('diesel', $distance);
        $sql = "INSERT INTO `subroute` (`ride_id`, `start`, `start_lat`, `start_long`, `end`, `end_lat`, `end_long`, `distance`, `price`, `time`, `co2`) VALUES ('{$ride_id}', '{$source}', '{$source_lat}', '{$source_lng}', '{$destination}', '{$destination_lat}', '{$destination_lng}', '{$distance}', '{$route_price}', {$time}, {$co2_data['value']})";
        $app->log->debug("inserting subroute  " . $sql);
        if ($mysqli->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Generating Unique uno ride id
     */
    private function generateUnoRideId() {
        return 'UNO'.date('Ymd').'RI'.date('his');
    }


    public function searchRides($app) {
        $search_params = $app->request()->params();
        $app->log->debug($search_params);
        $mysqli = $this->_dbh;
        $d = $search_params['departure_date'];
        $source_id = $search_params['source'];
        $destination_id = $search_params['destination'];
        $d = $search_params['departure_date'];
        $app->log->debug($d);
        $lower_limit_time = strtotime(substr($d, 0, -5));
        $upper_limit_time = strtotime('+5 day', $lower_limit_time);
        $app->log->debug($lower_limit_time . ' ------- ' . $upper_limit_time);
        $rideResults = [];
        $total_search_results = 0;
        //generate 5 days data keys with dates
        $rideResults[date('d-m-Y', $lower_limit_time)] = [];
        $rideResults[date('d-m-Y', strtotime('+1 day', $lower_limit_time))] = [];
        $rideResults[date('d-m-Y', strtotime('+2 day', $lower_limit_time))] = [];
        $rideResults[date('d-m-Y', strtotime('+3 day', $lower_limit_time))] = [];
        $rideResults[date('d-m-Y', strtotime('+4 day', $lower_limit_time))] = [];
        $sql = "SELECT * from ride_detail WHERE status = 'PUBLISHED' AND departure_date BETWEEN {$lower_limit_time} AND {$upper_limit_time};";
        $app->log->debug($sql);
        try
        {
            if($result = $mysqli->query($sql))
            {
                if($result->num_rows)
                {
                    $app->log->debug($result->num_rows);
                    //$vehicleResult = mysqli_fetch_all($result,MYSQLI_ASSOC);
                    while ($row = $result->fetch_assoc()) {
                        $row['preferences'] = json_decode($row['preferences']);
                        foreach ($rideResults as $key => $value) {
                            if(date('d-m-Y',$row['departure_date']) == $key) {
                                $date = date_create();
                                date_timestamp_set($date, $row['departure_date']);
                                $row['ride_date'] = date_format($date, 'Y-m-d H:i:s');
                                $row['ride_routes'] = $this->getRideRoutes($app, $row['id']);
                                $app->log->debug(date_format($date, 'Y-m-d H:i:s'));
                                $row['user_search_data'] = $this->prepareUserSearchData($app, $source_id, $destination_id, $row['price']);
                                array_push($rideResults[$key], $row);
                            }
                        }
                    }
                    foreach ($rideResults as $key => $value) {
                       $total_search_results += sizeof($rideResults[$key]);
                    }
                    $response['code'] = 200;
                    $response['results'] = $rideResults;
                    $response['error'] = 'false';
                    $response['total_search_results'] = $total_search_results;
                }
                else
                {
                    $response['code'] = 400;
                    $response['message'] = "error";
                    $response['error'] = 'true';
                }
            }
            else
            {

            }
        }
        catch(Exception $e)
        {
            $response['code'] = 500;
            $response['status'] = "MYSQL_ERROR";
            $response['vehicle_details'] = [];
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'true';
        }
        return $response;
    }


    public function getAllStopPointsOfRide($app, $ride_id) {
        $mysqli = $this->_dbh;
        $ride_locations = [];
        $sql = "SELECT ride_detail.id as ride_id, ride_detail.source_latitutde, ride_detail.source_longitude, ride_detail.destination_latitude, ride_detail.destinantion_longitude, subroute.id as sub_route_id, subroute.start_lat, subroute.start_long, subroute.end_lat, subroute.end_long FROM ride_detail LEFT JOIN subroute ON subroute.ride_id = ride_detail.id WHERE ride_detail.id='{$ride_id}'";
        $app->log->debug($sql);
        try
        {
            if($result = $mysqli->query($sql))
            {
                if($result->num_rows)
                {
                    $app->log->debug($result->num_rows);
                    $dest = [];
                    //$vehicleResult = mysqli_fetch_all($result,MYSQLI_ASSOC);
                    while ($row = $result->fetch_assoc()) {
                        $res = [];
                        $app->log->debug($row);
                        $res['lat'] = $row['start_lat'];
                        $res['lng'] = $row['start_long'];
                        $dest['lat'] = $row['end_lat'];
                        $dest['lng'] = $row['end_long'];
                        array_push($ride_locations, $res);
                    }
                    array_push($ride_locations, $dest);
                }
                else
                {
                    $response['error'] = 'true';
                }
            }
            else
            {
                $response['error'] = 'true';
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'true';
        }
        $app->log->debug($ride_locations);
        return $ride_locations;
    }

    public function publishRide($app, $m){
        $mysqli = $this->_dbh;
        $json = $app->request->getBody();

        $data = json_decode($json, true);
        $app->log->debug($data);
        $rideData = $data['ride'];
        $app->log->debug($rideData);

        $response = [];
        $user = new  User();

        $user->getUserId($rideData['clientID']);
        try {
            $sql =  "UPDATE ride_detail SET status ='PUBLISHED' WHERE uno_ride_id = '{$rideData['uno_ride_id']}'";
            if ($mysqli->query($sql) === true) {
                $response['code'] = 201;
                $response['status'] = "SUCCESS";
                $response['message'] = "Ride has been published successfully";
                $response['error'] = 'false';
                $response['uno_ride_id'] = $rideData['uno_ride_id'];
            } else {
                $response['code'] = 400;
                $response['status'] = "ERROR";
                $response['message'] = "Unable to publish your ride. Please try again";
                $response['error'] = 'true';
            }
        } catch (Exception $e) {
            $response['code'] = 400;
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'true';
        }
        return $response;
    }

    public function getRideRoutes ($app, $ride_id) {
        $mysqli = $this->_dbh;
        $routes_data= [];
        $ride_routes = [];
        $seat_offered = 0;
        $total_ride_distance = 0; $total_co2_emitted = 0;
        $sql = "SELECT subroute.*, ride_detail.no_of_seats_offered from subroute LEFT JOIN ride_detail ON ride_detail.id = subroute.ride_id  WHERE ride_id = {$ride_id} ";
        $app->log->debug($sql);
        try
        {
            if($result = $mysqli->query($sql))
            {
                if($result->num_rows)
                {
                    $app->log->debug($result->num_rows);
                    while ($row = $result->fetch_assoc()) {
                        settype($row['no_of_seats_offered'], "integer");
                        $seat_offered = $row['no_of_seats_offered'];
                        array_push($ride_routes, $row);
                        $total_ride_distance = $total_ride_distance + $row['distance'];
                        $total_co2_emitted = $total_co2_emitted + (float)$row['co2'];
                    }
                    $routes_data['routes'] = $ride_routes;
                    $routes_data['co2_emitted'] = round(($total_co2_emitted * ($seat_offered-1) )/1000);
                    $routes_data['distance'] = round($total_ride_distance);
                }
            }
        }
        catch(Exception $e)
        {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'true';
            $app->log->debug($response);
        }
        $app->log->debug($routes_data);
        return $routes_data;
    }

    public function prepareUserSearchData ($app, $source, $destination, $ride_price) {
        $util = new Utils();
        $source_location = $util->getLatLngFromAddressID($app, $source);
        $destination_location = $util->getLatLngFromAddressID($app, $destination);

        $app->log->debug($source_location);

        $app->log->debug($destination_location);
        $ride_distance_data = $util->get_distance_between_points($app, $source_location['lat'], $source_location['lng'], $destination_location['lat'], $destination_location['lng']);
        $ride_distance = round($ride_distance_data['travel_distance']);
        $ride_time = round($ride_distance_data['travel_time']);
        $ride_cost = round($ride_distance * $ride_price);

        return [
            "journey_distance" => $ride_distance,
            "journey_time" => $ride_time,
            "journey_cost" => $ride_cost,
            "journey_locations" => [
                "source" => [
                    "lat" => $source_location['lat'],
                    "lng" =>  $source_location['lng']
                ],
                "destination" => [
                    "lat" => $destination_location['lat'],
                    "lng" =>  $destination_location['lng']
                ]
            ]
        ];
    }
}