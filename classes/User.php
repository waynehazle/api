<?php

/**
 * Created by PhpStorm.
 * User: ola
 * Date: 12/22/15
 * Time: 1:09 AM
 */
require_once 'database/connection.php';
require_once 'classes/password.php';

/**
 * Class User
 */
class User extends Password
{
    /**
     * User constructor.
     */
    public function __construct(){
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }

    /**
     * This function will be used to register a particular user.
     * @param $app
     * @return array
     */
    public function registerUser ($app) {
        $response = array();
        $mysqli = $this->_dbh;
        $referral = new  Referral();
        $subscriber = new Subscriber();
        $response = array();
        $json = $app->request->getBody();
        $registerData = json_decode($json, true);
        $app->log->debug($registerData);
        //verifyRequiredParams(array('username', 'email', 'password'));
        try {
            $app->log->debug('checking user exist or not for' .$registerData['user']['email']);
            if ($this->isUserExists($registerData['user']['email'])) {
                $response['status'] = 'USER_ALREADY_EXISTED';
            } else {
                if (array_key_exists('referredBy', $registerData['user']) && $registerData['user']['referredBy'] != null) {
                    $isValidReferral = $referral->getUserIdByReferralCode($registerData['user']['referredBy']);
                    if($isValidReferral == 0) {
                        $response['status'] = 'INVALID_REFERRAL_CODE';
                        return $response;
                    }
                }
                $uno_userId = $this->generateUnoUserId();
                $api_key = $this->generateApiKey();
                $verificationToken = $this->generateVerificationToken();
                $authToken = $this->generateAuthToken();
                $hashsedPassword = $this->password_hash($registerData['user']['password'], PASSWORD_BCRYPT);
                $sql = "INSERT INTO users (`uno_userid`,`user_fullname`, `user_email`, `password`, `user_apikey`, `verification_code`, `authtoken`) VALUES ('{$uno_userId}', '{$registerData['user']['name']}', '{$registerData['user']['email']}', '{$hashsedPassword}', '{$api_key}', '{$verificationToken}', '{$authToken}')";
                $app->log->debug($sql);
                if ($mysqli->query($sql) === TRUE) {
                    $user_Id = $mysqli->insert_id;
                    $this->insertPersonalDetailsRow($user_Id, $registerData['user']['gender']);
                    $referral->add_new_referral_code($user_Id);
                    if (array_key_exists('referredBy', $registerData['user']) && $registerData['user']['referredBy'] != null) {
                        $referral->useReferralCode($registerData['user']['referredBy']);
                        $referral->insertReferralUsersMapping($user_Id, $referral->getUserIdByReferralCode($registerData['user']['referredBy']));
                    }
                    $app->log->debug('calling subscriber');
                    $response['internal_user_id'] = $user_Id;
                    $response['uno_user_id'] = $uno_userId;
                    $response['message'] = "Success. Registration successful.";
                    $response['error'] = 'false';
                    $response['status'] = 'USER_CREATED';
                } else {
                    $response['internal_user_id'] = 0;
                    $response['uno_user_id'] = 0;
                    $response['message'] = "Error. Registration failed.";
                    $response['error'] = 'true';
                    $response['status'] = 'USER_CREATION_FAILED';
                }
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $mysqli->close();
        return $response;
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) {
        $mysqli = $this->_dbh;
        $stmt = $mysqli->prepare("SELECT id from users WHERE `user_email` = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows;
    }

    private function getUserStatus($email, $app) {

        try {
            $mysqli = $this->_dbh;
            $app->log->debug($email);
            $sql = "SELECT status from users WHERE user_email = '{$email}' LIMIT 1";
            if ($result = $mysqli->query($sql)) {
                /* fetch object array */
                if($result->num_rows) {
                    while ($row = mysqli_fetch_assoc($result))
                        return ($row['status']);
                } else {
                    $response = [
                        'code'=>'400',
                        'message'=> 'Email is not registered with us.',
                        'status'=> 'ERROR'
                    ];
                    echoRespnse(400, $response);
                    $app->stop();
                }
            } else {
                $response = [
                    'code'=>'400',
                    'message'=> 'Unable to find the user',
                    'status'=> 'ERROR'
                ];
                echoRespnse(400, $response);
                $app->stop();
            }
            /* free result set */
            $result->close();
            /* close connection */
            $mysqli->close();
        } catch (Exception $e){
            $response = [
                'code'=>'400',
                'message'=>$e,
                'status'=> 'ERROR'
            ];
            echoRespnse(400, $response);
            $app->stop();
        }

    }

    /**
     * This function will be used to get user deatils for an email id.
     * @param $app
     * @param $email
     * @return array
     */
    public function getUserByEmail($app, $email) {
        $mysqli = $this->_dbh;
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE `user_email` = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            $response["error"] = true;
            $response["message"] = 'Sorry! Unable to map user to this email id.';
            echoRespnse(400, $response);
            $app->stop();
        }
    }

    /**
     * @param $app
     * @param $uno_user_id
     * @return array
     */
    public function getUserByUnoUserID($app, $uno_user_id) {
        $app->log->debug("fetching user with user id " . $uno_user_id);
        $mysqli = $this->_dbh;
        $stmt = $mysqli->prepare("SELECT * FROM users WHERE uno_userid = ?");
        $stmt->bind_param("s", $uno_user_id);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $app->log->debug($user);
            $stmt->close();
            return $user;
        } else {
            $response["error"] = true;
            $response["message"] = 'Sorry! Unable to map user to this id.';
            echoRespnse(400, $response);
            $app->stop();
        }
    }

    /**
     * @param $user_id
     * @return array|null
     */
    public function getApiKeyById($app,$user_id) {
        $mysqli = $this->_dbh;
        $stmt = $mysqli->prepare("SELECT user_apikey FROM early_users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            $api_key = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $api_key;
        } else {
            $response["error"] = true;
            $response["message"] = 'Sorry! Unable to find user with this email id.';
            echoRespnse(400, $response);
            $app->stop();
        }
    }

    /**
     * @param $unoUserID
     * @return array|null
     */
    public function getUserId($unoUserID)
    {
        $app = \Slim\Slim::getInstance();
        $app->log->debug("inside getUserId: " . $unoUserID);
        $mysqli = $this->_dbh;
        $sql = "SELECT id from users WHERE uno_userid = '{$unoUserID}' LIMIT 1";
        $app->log->debug($sql);
        if ($result = $mysqli->query($sql)) {
            /* fetch object array */
            if ($result->num_rows) {
                while ($row = mysqli_fetch_assoc($result))
                    return ($row['id']);
            } else {
                $response = [
                    'code' => '400',
                    'message' => 'Unable to find user',
                    'status' => 'ERROR'
                ];
                echoRespnse(400, $response);
                $app->stop();
            }
        } else {
            $response = [
                'code' => '400',
                'message' => 'Unable to find the user',
                'status' => 'ERROR'
            ];
            echoRespnse(400, $response);
            $app->stop();
        }
    }

    public function getUser($app, $unoUserID)
    {
        $app->log->debug("inside getUserId: " . $unoUserID);
        $mysqli = $this->_dbh;
        $sql = "SELECT * from users WHERE uno_userid = '{$unoUserID}' LIMIT 1";
        $app->log->debug($sql);
        if ($result = $mysqli->query($sql)) {
            /* fetch object array */
            if ($result->num_rows) {
                while ($row = mysqli_fetch_assoc($result))
                    return ($row);
            } else {
                $response = [
                    'code' => '400',
                    'message' => 'Unable to find user',
                    'status' => 'ERROR'
                ];
                echoRespnse(400, $response);
                $app->stop();
            }
        } else {
            $response = [
                'code' => '400',
                'message' => 'Unable to find the user',
                'status' => 'ERROR'
            ];
            echoRespnse(400, $response);
            $app->stop();
        }
    }


    public function getUserByEmailId($app, $email)
    {
        $app = \Slim\Slim::getInstance();
        $app->log->debug("inside user: " . $email);
        $mysqli = $this->_dbh;
        $sql = "SELECT * from users WHERE user_email = '{$email}' LIMIT 1";
        $app->log->debug($sql);
        if ($result = $mysqli->query($sql)) {
            /* fetch object array */
            if ($result->num_rows) {
                while ($row = mysqli_fetch_assoc($result))
                    return ($row);
            } else {
                $response = [
                    'code' => '400',
                    'message' => 'Unable to find user',
                    'status' => 'ERROR'
                ];
                echoRespnse(400, $response);
                $app->stop();
            }
        } else {
            $response = [
                'code' => '400',
                'message' => 'Unable to find the user',
                'status' => 'ERROR'
            ];
            echoRespnse(400, $response);
            $app->stop();
        }
    }

    /**
     * @param $internal_user_id
     * @return array|null
     */
    public function getUNOUserId($internal_user_id) {
        $mysqli = $this->_dbh;
        $stmt = $mysqli->prepare("SELECT uno_userid FROM early_users WHERE id = ?");
        $stmt->bind_param("s", $internal_user_id);
        if ($stmt->execute()) {
            $uno_user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $uno_user_id;
        } else {
            return NULL;
        }
    }


    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /**
     * Generating random Unique MD5 String for user verification token
     */
    private function generateVerificationToken() {
        return bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
    }

    /**
     * Generating random Unique MD5 String for user auth token
     */
    private function generateAuthToken() {
        return bin2hex(mcrypt_create_iv(20, MCRYPT_DEV_URANDOM));
    }

    /**
     * Generating Unique unouserid
     */
    private function generateUnoUserId() {
        return 'UNO'.date('Ymd').'US'.date('his');
    }

    /**
     * This function returns the user's password hash.
     * @param $email
     * @param $app
     * @return string
     */
    private function get_user_hash($email, $app){
        try {
            $mysqli = $this->_dbh;
            $app->log->debug($email);
            $sql = "SELECT password from users WHERE user_email = '{$email}' LIMIT 1";
            if ($result = $mysqli->query($sql)) {
                /* fetch object array */
                if($result->num_rows) {
                    while ($row = mysqli_fetch_assoc($result))
                        return ($row['password']);
                } else {
                    $response = [
                        'code'=>'400',
                        'message'=> 'Email is not registered with us.',
                        'status'=> 'ERROR'
                    ];
                    echoRespnse(400, $response);
                    $app->stop();
                }
            } else {
                $response = [
                    'code'=>'400',
                    'message'=> 'Unable to find the user',
                    'status'=> 'ERROR'
                ];
                echoRespnse(400, $response);
                $app->stop();
            }
            /* free result set */
            $result->close();
            /* close connection */
            $mysqli->close();
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * this function used to login a user into the system and also set the session.
     * @param $app
     * @return array
     */
    public function login($app){
        $response = array();
        $json = $app->request->getBody();
        $app->log->debug(gettype($json));
        $loginData = json_decode($json, true);
        $app->log->debug($loginData);
        $app->log->debug($loginData['user']['password']);
        $password = $loginData['user']['password'];
        $email = $loginData['user']['email'];
        //verifyRequiredParams(array('email', 'password'));
        $passwordHash = $this->get_user_hash($email, $app);
        $app->log->debug("user results is");
        $app->log->debug($passwordHash);

        $app->log->debug("password match results :" . $this->password_verify($password,$passwordHash, $app));
        if(($this->password_verify($password,$passwordHash, $app)) <= 0){
            $user = $this->getUserByEmailId($app, $email);
            $app->log->debug($user);
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $user['user_fullname'];
            $_SESSION['userID'] = $user['id'];
            $_SESSION['authToken'] = $user['authtoken'];
            $_SESSION['clientId'] = $user['uno_userid'];
            $_SESSION['apiKey'] = $user['user_apikey'];
            $profile_data = $this->getCompleteUserProfileData($user['uno_userid'], $app);
            if (isset($profile_data['rating']) && $profile_data['rating'] != null) {
                $profile_data['rating'] = number_format((float)$profile_data['rating'], 1, '.', '');
            } else {
                $profile_data['rating'] = 0;
            }
            $app->log->debug($profile_data);
            return [
                'code'=> 200,
                'message'=> 'login successful',
                'status' => 'LOGIN_SUCCESS',
                'clientId' => $user['uno_userid'],
                'authToken' => $user['authtoken'],
                'apiKey' => $user['user_apikey'],
                'profile_data' => $profile_data
            ];
        } else {
            return [
                'code'=>400,
                'message'=> 'Login failed. Invalid username & password.',
                'status'=> 'LOGIN_FAILED'
            ];
        }
    }

    /**
     * This function will be called to logout the user from the application.
     * @param $app
     * @return array
     */
    public function logout($app){
        $_SESSION = array();
        session_destroy();
        $app->deleteCookie('uno_session');
        return $response = [
            'status' => 'LOGOUT_SUCCESS'
        ];
    }

    /**
     * THis function checks if a user is logged In or not.
     * @return bool
     */
    public static function is_logged_in(){
        if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
            return true;
        } else {
            return false;
        }
    }

    public function deactivateUser ($app) {
        $response = array();
        $json = $app->request->getBody();
        $userData = json_decode($json, true);
        $email = $userData['email'];
        $unoUserId = $userData['uno_userid'];
        $mysqli = $this->_dbh;
        verifyRequiredParams(array('email', 'uno_userid'));
        try {
            $app->log->debug('deactivating user validity with params ' . $email .' and '. $unoUserId);
            $userStatus = $this->getUserStatus($email, $app);
            if ($userStatus === 'ACTIVE') {
                $sql = "UPDATE users SET status = 'DEACTIVATED' WHERE user_email='{$email}' AND uno_userid='{$unoUserId}'";
                $mysqli->query($sql);
                $app->log->debug('affected rows for verification code'. $mysqli->affected_rows);
                if ($mysqli->affected_rows) {
                    return [
                        'status' => 'USER_DEACTIVATED'
                    ];
                } else {
                    return [
                        'status' => 'DEACTIVATION_FAILED'
                    ];
                }
            } else if ($userStatus === 'DEACTIVATED') {
                return [
                    'status' => 'ALREADY_DEACTIVATED'
                ];
            } else {
                return [
                    'status' => 'DEACTIVATION_FAILED'
                ];
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $mysqli->close();
        return $response;
    }

    public function activateUser ($app) {
        $response = array();
        $json = $app->request->getBody();
        $userData = json_decode($json, true);
        $email = $userData['email'];
        $unoUserId = $userData['uno_userid'];
        $mysqli = $this->_dbh;
        verifyRequiredParams(array('email', 'uno_userid'));
        try {
            $app->log->debug('deactivating user validity with params ' . $email .' and '. $unoUserId);
            $userStatus = $this->getUserStatus($email, $app);
            if ($userStatus === 'DEACTIVATED') {
                $sql = "UPDATE users SET status = 'DEACTIVATED' WHERE user_email='{$email}' AND uno_userid='{$unoUserId}'";
                $mysqli->query($sql);
                $app->log->debug('affected rows for verification code'. $mysqli->affected_rows);
                if ($mysqli->affected_rows) {
                    return [
                        'status' => 'USER_ACTIVATED'
                    ];
                } else {
                    return [
                        'status' => 'ACTIVATION_FAILED'
                    ];
                }
            } else if ($userStatus === 'ACTIVE') {
                return [
                    'status' => 'ALREADY_ACTIVATED'
                ];
            } else {
                return [
                    'status' => 'ACTIVATION_FAILED'
                ];
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $mysqli->close();
        return $response;
    }

    /**
     * This function will be used to enter the empty row in user personal details on registration so that we can update
     * the details in the future.
     * @param $user_id
     * @param $gender
     */
    private function insertPersonalDetailsRow ($user_id, $gender) {
        $mysqli = $this->_dbh;
        $sql = "INSERT INTO user_personal_details (userid, gender) VALUES ('{$user_id}', '{$gender}')";
        $result = $mysqli->query($sql);
    }

    public function sendOTP($userid, $phone, $app)
    {
        //verify USER by clientID and apiKey in $app

        //generate OTP
        $otp = mt_rand(1000, 9999);

        //send SMS
        $message = "Unoride.com, ride sharing simplified. Your OTP is: " . $otp;
        //$service_response = sendSMS($phone, $message);
        $util = new Utils();

        //$service_response = $util->sendSMS($app, $sms, $phone, '919425787381', $message);
        $service_response = json_encode('test');
        //Insert details in table
        $mysqli = $this->_dbh;
        $response = array();
        //$sql = "INSERT INTO phoneNumVerification VALUES ('{$uno_userid}', '{$otp}', 0, '{$service_response}' )";
        $sql = "INSERT INTO phoneNumVerification (`userid`, `otp`, `phone_verified`, `service_reply`) VALUES ('{$userid}', '{$otp}', 0, '{$service_response}' )";
        $app->log->debug($sql);

        try {
            if ($mysqli->query($sql) === true)
                return [
                    'status' => 'OTP_SAVE_IN_DB_SUCCESS',
                    'OTP' => $otp
                ];
            else
                return [
                    'status' => 'OTP_SAVE_IN_DB_FAIL',
                    'OTP' => 0
                ];
        } catch (Exception $e) {
            $response["error"] = true;
            $response["message"] = 'Sorry' . $e->getMessage();
            echoRespnse(400, $response);
        }
        return $response;
    }

    /**
     * This function will be used to insert the personal details based on the params passed to it.
     * @param $app
     * * @param $sms
     * @return string
     */
    public function updateUserProfile ($app) {
        $mysqli = $this->_dbh;
        $response = array();
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        $userData = $data['user'];
        $clientID = $userData['clientID'];
        $userID = $this->getUserId($clientID);
        $app->log->debug($userID);
        $app->log->debug('user found with id : ' . $userID);
        $otpResult = array();
        $otpResult['OTP'] = 0;
        $status = 'PROFILE_UPDATED';
        if ($userID) {
            try {
                $uid = $userID;
                $app->log->debug('username is ' . array_key_exists('firstname', $userData));
                $firstname = array_key_exists('firstname', $userData) ? $userData['firstname'] : '';
                $lastname = array_key_exists('lastname', $userData) ? $userData['lastname'] : '';
                $gender = array_key_exists('gender', $userData) ? $userData['gender'] : '';
                $age = array_key_exists('age', $userData) ? $userData['age'] : '';
                $phone = array_key_exists('phone', $userData) ? $userData['phone'] : '';
                $status = $phone != '' ? 'PHONE_UPDATED' : 'PROFILE_UPDATED';
                if ($status == 'PHONE_UPDATED') {
                    $otpResult = $this->sendOTP($uid, $phone, $app);
                }
                $query = 'UPDATE user_personal_details SET ';
                $app->log->debug('firstname ' . $firstname);
                if ($firstname != '') $query .= 'firstname="'.$firstname.'", ';
                if ($lastname != '') $query .= 'lastname="'.$lastname.'", ';
                if ($gender != '') $query .= 'gender="'.$gender.'", ';
                if ($age != '') $query .= 'age="'.$age.'", ';
                if ($phone != '') $query .= 'phone="'.$phone.'", ';
                $query .= "status = '{$status}' where userid = '{$uid}'";
                $app->log->debug($query);
                $mysqli->query($query);
                $app->log->debug('affected rows for user personal information '. $mysqli->affected_rows);
                if ($mysqli->affected_rows) {
                    return [
                        'status' => $status,
                        'OTP' => $otpResult['OTP']
                    ];
                } else {
                    return [
                        'status' => 'UPDATE_FAILED'
                    ];
                }
            } catch (Exception $e) {
                return [
                    'status' => 'MYSQL_ERROR',
                    'message' => $e->getMessage()
                ];
            }
        } else {
            return [
                'status' => 'INVALID_CLIENT_ID'
            ];
        }

        $mysqli->close();
        //return $response;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateForgotPasswordToken() {
        return bin2hex(mcrypt_create_iv(24, MCRYPT_DEV_URANDOM));
    }

    public function forgotPassword($app, $m)
    {
        $json = $app->request->getBody();
        $pwdData = json_decode($json, true);
        $app->log->debug($pwdData);
        $response = array();
        $mysqli = $this->_dbh;
        try {
            $app->log->debug('checking user exist or not for ' . $pwdData['user']['email']);
            if ($this->isUserExists($pwdData['user']['email'])) {
                $email = $pwdData['user']['email'];
                $pwdToken = $this->generateForgotPasswordToken();
                $sql = "UPDATE users SET forgot_password_token = '{$pwdToken}' WHERE user_email ='{$email}'";
                $app->log->debug($sql);
                if ($mysqli->query($sql) === true) {
                    $user = $this->getUserByEmail($app, $email);
                    $app->log->debug($user);
                    $m->sendUpdatePasswordMail($app, $email, $user['user_fullname'], $pwdToken);
                    $response['status'] = "RESET_PASSWORD_MAIL_SENT";
                    $response['message'] = "Reset password mail sent.";
                    $response['error'] = 'false';
                }
            } else {
                $response['status'] = "USER_DOES_NOT_EXIST";
                $response['message'] = "Sorry, please enter correct email id.";
                $response['error'] = 'true';
            }
        } catch (Exception $e) {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry, Query failed. ".$e->getMessage();
            $response['error'] = 'true';
        }

        return $response;
    }

    public function savePassword($app, $m)
    {
        $json = $app->request->getBody();
        $pwdData = json_decode($json, true);
        $app->log->debug("hello");
        $app->log->debug($pwdData['password']);
        $response = array();
        $email = $app->request()->post('email');
        $password = $app->request()->post('password');
        $token = $app->request()->post('token');
        $mysqli = $this->_dbh;
        try {
            $sql = "SELECT forgot_password_token FROM users WHERE user_email = '{$email}'";
            $app->log->debug($sql);
            if ($result = $mysqli->query($sql)) {
                if ($result->num_rows) {
                    $row = mysqli_fetch_assoc($result);
                    if (isset($row['forgot_password_token']) && $row['forgot_password_token'] == $token) {
                        $hashedPassword = $this->password_hash($password, PASSWORD_BCRYPT);
                        $sql = "UPDATE users SET password = '{$hashedPassword}', forgot_password_token = null WHERE user_email ='{$email}' AND forgot_password_token = '{$token}'";
                        $app->log->debug($sql);

                        if ($mysqli->query($sql) === true) {
                            $user = $this->getUserByEmail($app, $email);
                            $app->log->debug($user);
                            // $m->sendSavePasswordMail($app, $email, $user['user_fullname']);
                            $response['status'] = "PASSWORD_SAVED";
                            $response['message'] = "Password saved.";
                            $response['error'] = 'false';
                        } else {
                            $response['status'] = "PASSWORD_SAVE_FAILED";
                            $response['message'] = "Sorry, password save failed. Try again.";
                            $response['error'] = 'true';
                        }
                    } else {
                        $response['status'] = "UNAUTHORIZED_ACCESS";
                        $response['message'] = "Forgot password tokes is not set for this user.";
                        $response['error'] = 'true';
                    }
                }
            }

        } catch (Exception $e) {
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry, Query failed. " . $e->getMessage();
            $response['error'] = 'true';
        }
        return $response;
    }


    public function validPasswordChangeRequest($app, $email, $password_token) {
        $mysqli = $this->_dbh;
        $app->log->debug($password_token);
        $stmt = $mysqli->prepare("SELECT * from users WHERE user_email = ? AND forgot_password_token = ?");
        $stmt->bind_param("ss", $email, $password_token);
        if ($stmt->execute()) {
            $result = $stmt->get_result()->fetch_assoc();
            $app->log->debug(sizeof($result));
            $stmt->close();
            if (sizeof($result)) {
                return 'VALID';
            } else {
                return 'INVALID';
            }

        } else {
            return 'INVALID';
        }
    }

    public function getCompleteUserProfileData ($uno_user_id, $app) {
        $response = [];
        try {
            $mysqli = $this->_dbh;
            $app->log->debug("fetching user details with uno use id : #{$uno_user_id}");
            $sql = "SELECT users.user_email, users.user_fullname AS fullname, referral_codes.code AS referral_code, user_personal_details.firstname, user_personal_details.lastname, user_personal_details.phone, user_personal_details.gender, user_personal_details.age, user_personal_details.dl_number, users.status, avg(ratings.rating) as rating from users LEFT JOIN referral_codes ON referral_codes.associate_user = users.id LEFT JOIN user_personal_details ON user_personal_details.userid = users.id LEFT JOIN ratings ON ratings.user_id = user_personal_details.userid WHERE uno_userid = '{$uno_user_id}' AND users.status = 'ACTIVE';";
            $app->log->debug($sql);
            if ($result = $mysqli->query($sql)) {
                if ($result->num_rows) {
                    $response = mysqli_fetch_assoc($result);
                }
            }
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function getRideCreateData ($app) {
        $json = $app->request->getBody();
        $app->log->debug($json);
        $data = json_decode($json, true);
        $userData = $data['user'];
        $vehicle = new Vehicle();
        $response = [];
        try {
            $uno_user_id = $userData['clientID'];
            $vehicle_data = $vehicle->getAllVehicles($app, $uno_user_id);
            $makers = $vehicle->getMakers($app);
            $response['vehicle_data'] = $vehicle_data;
            $response['makers'] = $makers;
        } catch (Exception $e) {
            $response['message'] = $e->getMessage();
            echoRespnse(400, ["error"=> true, "message"=> "Unable to find the vehicle details."]);
            $app->stop();
        }
        return $response;
    }

    public function getUserProfilePic($uno_user_id, $app)
    {

        $mysqli = $this->_dbh;
        $userId = $this->getUserId($uno_user_id);
        $sql = "SELECT profile_pic FROM user_personal_details WHERE userid = {$userId}";
        if ($result = $mysqli->query($sql)) {
            $response = mysqli_fetch_assoc($result);
            $image = file_get_contents("uploads/profile_pics/".$response['profile_pic']);
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $app->response->header('Content-Type', 'content-type: ' . $finfo->buffer($image));
            echo $image;
        } else {
            echoRespnse(400, ['error'=> true, 'message' => "No image found"]);
        }
    }

    public function insertProfilePic($uno_user_id, $profile_pic){
        $mysqli = $this->_dbh;
        $userId = $this->getUserId($uno_user_id);
        $sql =  "UPDATE user_personal_details SET profile_pic ='{$profile_pic}' WHERE userid = {$userId}";
        if ($mysqli->query($sql) === true) {
            echo "profile pic inserted";
            return true;
        } else {
            echo "profile pic insert failed";
            return false;
        }
    }

    public function submitUserRating($app){
        $response = [];
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        $rating_data = $data['rating'];
        $app->log->debug($rating_data);
        $mysqli = $this->_dbh;
        $rater_id = $this->getUserId($rating_data['clientID']);
        $user_id = $this->getUserId($rating_data['user_id']);
        if ($rater_id != $user_id) {
            try {
                $sql = "INSERT INTO ratings (user_id, rater_id, rating) VALUES ('{$user_id}', '{$rater_id}', '{$rating_data['rating']}')";
                if ($mysqli->query($sql) === true) {
                    $response['code'] = 201;
                    $response['message'] = "Rating has been submitted.";
                } else {
                    $response['code'] = 400;
                    $response['message'] = "Unable to submit the rating.";
                }
            } catch (Exception $e) {
                $response['code'] = 400;
                $response['message'] = "Request failed with message:". $e;
            }
        } else {
            $response['code'] = 400;
            $response['message'] = "Sorry, You can't rate yourself as you are already awesome. :)";
        }

        return $response;
    }

    public function getUserRating($app, $uno_user_id){
        $response = [];
        $json = $app->request->getBody();
        $mysqli = $this->_dbh;
        $user_id = $this->getUserId($uno_user_id);
        try {
            $sql = "SELECT AVG(rating) as rating FROM ratings WHERE user_id = {$user_id};";
            if ($result = $mysqli->query($sql)) {
                /* fetch object array */
                if ($result->num_rows) {
                    while ($row = mysqli_fetch_assoc($result))
                        $response = [
                            'code' => '200',
                            'rating' => number_format((float)$row['rating'], 1, '.', '')
                        ];
                } else {
                    $response = [
                        'code' => '200',
                        'rating' => 0
                    ];
                }
            } else {
                $response = [
                    'code' => '400',
                    'message' => 'Unable to process the request',
                ];
            }
        } catch (Exception $e) {
            $response['code'] = 400;
            $response['message'] = "Request failed with message:". $e;
        }
        return $response;
    }
}