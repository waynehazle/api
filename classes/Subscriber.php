<?php
/**
 * Created by PhpStorm.
 * User: Arpit
 * Date: 03-Jan-16
 * Time: 1:20 PM
 */

require_once 'database/connection.php';

class Subscriber extends Db
{
    public function __construct()
    {
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }

    public function subscribeUser($userID)
    {

        $app = \Slim\Slim::getInstance();
        $app->log->debug('i m called');
        $mysqli = $this->_dbh;
        $response = array();
        $sql = "INSERT INTO subscribers (`user_id`, `subscribed`, `reason`) VALUES ('{$userID}', 'YES', 'Early Subscriber')";
        $app->log->debug($sql);
        try
        {
            if($mysqli->query($sql) === true)
                return 'USER_SUBSCRIBE_SUCCESS';
            else
                return 'USER_SUBSCRIBE_FAIL';
        }
        catch(Exception $e)
        {
            $response["error"] = true;
            $response["message"] = "Sorry. ". $e->getMessage();
            echoRespnse(400, $response);
            $app->stop();
        }
        return $response;
    }

    public function unSubscribeUser($user_id)
    {
        $app = \Slim\Slim::getInstance();
        $mysqli = $this->_dbh;
        $sql = "UPDATE subscribers SET subscribed='NO' WHERE user_id='{$user_id}'";
        $mysqli->query($sql);
        if ($mysqli->affected_rows) {
            return 'USER_UNSUBSCRIBED';
        } else {
            return 'UNSUBSCRIPTION_FAILED';
        }
    }
}