<?php

/**
 * Created by PhpStorm.
 * User: ola
 * Date: 2/28/16
 * Time: 2:27 PM
 */
require_once "database/connection.php";

class Bracket
{
    public function __construct(){
        $db = Db::getInstance();
        $this->_dbh = $db->getConnection();
    }

    public function getAllFeeds ($app) {
        $response = [];
        $feeds = [];
        $mysqli = $this->_dbh;
        $sql = "SELECT DISTINCT TIMESTAMPDIFF(MINUTE, winner.created, NOW()) AS createdBefore, TIMESTAMPDIFF(MINUTE, winner.updated_on, NOW()) AS updatedBefore, winner.id, winner.category_id, winner.owner_id,
  winner.won_item_id, winner.title AS bracket_title, winner.created, winner.round_count AS roundCounts, winner.round_length, winner.bracket_description,
  headerPic_URL AS bracket_pic, user.id AS user_id, user.first_name, user.last_name, item.title, avatar.location as avatar_path, current_round_number FROM winner
  LEFT JOIN user ON winner.owner_id = user.id LEFT JOIN item ON winner.won_item_id = item.id LEFT JOIN avatar ON user.id = avatar.user_id LEFT JOIN bracket_current_round ON winner.id = bracket_current_round.bracket_id
WHERE bracket_current_round.status = 'live' ORDER BY updatedBefore;";
        $app->log->debug($sql);
        try
        {
            if($result = $mysqli->query($sql))
            {
                if($result->num_rows)
                {
                    $app->log->debug($result->num_rows);
                    while ($row = $result->fetch_assoc()) {
                        array_push($feeds, $row);
                    }
                    $response['code'] = 200;
                    $response['brackets_count'] = sizeof($feeds);
                    $response['feeds'] = $feeds;
                    $response['status'] = "SUCCESS";
                    $response['message'] = "Total brackets ". sizeof($feeds);
                } else {
                    $response['code'] = 400;
                    $response['status'] = "ERROR";
                    $response['feeds'] = $feeds;
                    $response['message'] = "Sorry. no results found";
                    $app->log->debug($response);
                }
            }
        }
        catch(Exception $e)
        {
            $response['code'] = 500;
            $response['status'] = "MYSQL_ERROR";
            $response['message'] = "Sorry. Query failed: ". $e->getMessage();
            $response['error'] = 'true';
            $app->log->debug($response);
        }
        return $response;
    }
}