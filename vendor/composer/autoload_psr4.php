<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Slim\\Views\\' => array($vendorDir . '/slim/views'),
    'Slim\\Middleware\\' => array($vendorDir . '/christianklisch/slim-minify/src/Slim/Middleware', $vendorDir . '/tuupola/slim-jwt-auth/src'),
    'Slim\\HttpCache\\' => array($vendorDir . '/slim/http-cache/src'),
    'Slim\\Csrf\\' => array($vendorDir . '/slim/csrf/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GeometryLibrary\\' => array($vendorDir . '/alexpechkarev/geometry-library'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'CorsSlim\\' => array($vendorDir . '/palanik/corsslim'),
);
